import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { HttpService } from '../shared/http.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-myproduct-edit',
  templateUrl: './myproduct-edit.page.html',
  styleUrls: ['./myproduct-edit.page.scss'],
})
export class MyproductEditPage implements OnInit {

  constructor(private http: HttpService, private router: Router, private route: ActivatedRoute) {

    route.params.subscribe(val => {
      this.getCategoryList()
      this.getSubcategoryList();
      this.getUnit();
      this.LoadReadData();
      this.appendReadData();


    })


  }


  LoadReadData() {
    this.route.queryParams.subscribe(params => {
      //console.log(params.product_name);
      this.product_name = params.product_name;
      this.tbid_value = params.tbid;
    }
    );
  }


  ngOnInit() {
  }



  appendReadData() {
    const p = this.product_name;
    //console.log(p);
    this.http.get('/read_one_product?p=' + p).subscribe((response: any) => {

      this.description = response.records.description;
      this.Category = response.records.category;
      this.subcategory = response.records.subcategory;
      this.productname = response.records.product_name;
      this.cost = response.records.cost;
      this.DisplayImage = response.records.product_image;
      this.tbid = response.records.tbid;


    }, (error: any) => {
      console.log(error);
    }
    );
  }

  product_name: any = ""
  tbid: any = ""
  DisplayImage: any = "";
  tbid_value: any = "";
  displayreaddata = [];
  readunit: [];
  showProducts = [];
  public date: string = new Date().toISOString();
  Category: any = '';
  subcategory: any = '';
  productname: any = '';
  description: any = '';
  cost: any = '';
  showimage = '';
  showImageDisplay = false;
  categoryName: any = '';
  subcategoryName: any = '';
  categoryList: any = [];
  subcategoryList: any = [];
  hideImageDisplay = true;
  PopupModel: any = false;
  subcategoryPopupModel: any = false;
  readunitvalue: any = "";

  showselecteditems() {
    const updateItems = {
      tbid: this.tbid_value,
      category: this.Category,
      subcategory: this.subcategory,
      product_name: this.productname,
      description: this.description,
      cost: this.cost + this.readunitvalue
    }
    console.log(updateItems);
    
  }

  backToprivious() {
    this.PopupModel = false;
    this.Category = ''

  }

  deleteProduct() {
    const obj = {
      tbid: this.tbid_value
    }
    this.http.post('/delete_product', obj).subscribe((response: any) => {
      //console.log(response);
      if (response.success == "true") {
        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 1000,
          timerProgressBar: true,
          didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
          }
        })

        Toast.fire({
          icon: 'success',
          title: 'Deleted successfully'
        })
        this.router.navigate(['/myproducts'])
      }


    }, (error: any) => {
      //console.log(error);
    }
    );

  }


  NavigatetoNotification() {
    this.router.navigate(['/notification'])
  }


  ScBackToprivious() {
    this.subcategoryPopupModel = false;
    this.subcategory = ''

  }

  addproduct() {

    this.router.navigate(['/myproducts'])
  }
  createCategory() {
    this.PopupModel = false;
    const catData = {
      category_name: this.categoryName,
      created_at: this.date
    }

    this.http.post('/create_category', catData).subscribe((response: any) => {
      //console.log(response);

      if (response.success == "true") {
        this.Category = ''
        this.categoryName = ''
        this.getCategoryList()


      }

    }, (error: any) => {
      //console.log(error);
    }
    );

  }


  getUnit() {
    this.http.get('/read_unit').subscribe((response: any) => {
      //console.log(response.records);
      this.readunit = response.records;


    }, (error: any) => {
      //console.log(error);
    }
    );

  }


  createSubcategory() {
    this.subcategoryPopupModel = false;
    const subcatData = {
      category: this.Category,
      subcategory_name: this.subcategoryName

    }

    this.http.post('/create_subcategory', subcatData).subscribe((response: any) => {
      //console.log(response);

      if (response.success == "true") {
        //console.log("test");
        this.Category = ''
        this.subcategory = ''
        this.subcategoryName = ''
        this.getSubcategoryList()


      }

    }, (error: any) => {
      console.log(error);
    }
    );

  }

  getCategoryList() {

    this.http.get('/read_category',).subscribe((response: any) => {
      //console.log(response.records);

      this.categoryList = response.records
      console.log(response.records);
      //console.log(this.categoryList);


    }, (error: any) => {
      //console.log(error);
    }
    );
  }

  getSubcategoryList() {
    this.http.get('/read_subcategory',).subscribe((response: any) => {
      //console.log(response.records);
      this.subcategoryList = response.records
      //console.log(response.records);
      //console.log(this.subcategoryList);

    }, (error: any) => {
      console.log(error);
    }
    );
  }



  backToMyproducts() {
    this.router.navigate(['/myproducts'])
  }

  isvisible: any = false;

  hidepopup() {
    this.isvisible = false;
  }
  showPopup() {
    this.isvisible = true;
    this.http.get('/product_image_list').subscribe((response: any) => {
      console.log(response);
      this.showProducts = response.records;

    }, (error: any) => {
      console.log(error);

    }

    )
  }
  displayImage(i) {
    this.showImageDisplay = true;
    this.showimage = this.showProducts[i];
    console.log(this.showimage);
    this.isvisible = false;
    this.hideImageDisplay = false;
  }
}
