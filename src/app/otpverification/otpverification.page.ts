import { Component, OnInit } from '@angular/core';
import { HttpService } from '../shared/http.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { ToastController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-otpverification',
  templateUrl: './otpverification.page.html',
  styleUrls: ['./otpverification.page.scss'],
})
export class OtpverificationPage implements OnInit {

  constructor(private router: Router, private http: HttpService,
    private toastCtrl: ToastController, route: ActivatedRoute) { }

  ngOnInit() {
    console.log(this.parsedObj);
  }
  mail: any = atob(localStorage.getItem("mailId")); 
  parsedObj: any = JSON.parse(this.mail);
  
  

  OneTimePassword: any = "";
  phonenumber: any = "";
  OTPSent: any = false;

  sendOtp() {
    alert("hello");
    const obj = {
      store_number: this.phonenumber
    }
    this.http.post('/seller_send_otp', obj).subscribe((response: any) => {
      this.OTPSent = true;
      console.log(response);

    }, (error: any) => {
      console.log(error);
    }
    );
  }

  verifyOTP() {
    alert(this.parsedObj.email);
    const obj = {
      email_id: this.parsedObj.email,
      verify_otp: this.OneTimePassword
    }
    console.log(obj);
    
    this.http.post('/seller_verify_otp', obj).subscribe((response: any) => {
      console.log(response);
      if (response.success == "true") {
        this.router.navigate(['/signinpage'])
      } else {
        this.OneTimePassword = "";
        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 1000,
          timerProgressBar: true,
          didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
          }
        })

        Toast.fire({
          icon: 'error',
          title: 'Incorrect OTP'
        })
      }
    }, (error: any) => {
      console.log(error);
    }
    );
  }
}
