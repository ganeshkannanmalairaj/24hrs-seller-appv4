import { Component, OnInit } from '@angular/core';
import { HttpService } from '../shared/http.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-tab4',
  templateUrl: 'tab4.page.html',
  styleUrls: ['tab4.page.scss']
})
export class Tab4Page {
  route: any;

  constructor(private router: Router, private http: HttpService,
    private toastCtrl: ToastController, route: ActivatedRoute) {
    route.params.subscribe(val => {
      this.isvisible = false;
      this.cardVisible = true;
      this.getCategoryList()
      this.getSubcategoryList()
      this.getProductList()
      this.getOfferList();
      // this.orderObj()

    });

  }

  ngOnInit() {


  }
  categoryList: any;
  subcategoryList: any = [];
  offerList: any = ""
  productList: any;
  cardVisible: any = true;
  isvisible: any = false;

  Category: any = '';
  subcategory: any = '';
  productName: any = '';
  offer: any = '';
  otheroffer: any = ''
  totalCost: any = '';
  offerPrice: any = '';
  offerTime: any = '';
  description: any = '';

  orderObjRes: any = [];
  backToPrivious() {
    this.cardVisible = true;
    this.isvisible = false;
  }

  offerPage() {
    this.cardVisible = false;
    this.isvisible = true;
  }
  NavigatetoNotification() {
    this.router.navigate(['/notification'])
  }

  
  addoffer() {
    const productData = {
      category: this.Category,
      subcategory: this.subcategory,
      product: this.productName,
      description: this.description,
      offer: this.offer,
      other_offer: this.otheroffer,
      total_cost: this.totalCost,
      offer_price: this.offerPrice,
      offer_time: this.offerTime


    }

    this.http.post('/create_offer', productData).subscribe((response: any) => {
      console.log(response);
      if (response.success == "true") {
        console.log(response);
      }
    }, (error: any) => {
      console.log(error);
    }
    );



  }


  getCategoryList() {

    this.http.get('/read_category',).subscribe((response: any) => {

      this.categoryList = response.records
      console.log(response.records);

    }, (error: any) => {
      console.log(error);
    }
    );
  }


  getSubcategoryList() {
    this.http.get('/read_subcategory',).subscribe((response: any) => {
      console.log(response.records);
      this.subcategoryList = response.records
      console.log(response.records);
      console.log(this.subcategoryList);

    }, (error: any) => {
      console.log(error);
    }
    );
  }






  getProductList() {
    this.http.get('/read_product',).subscribe((response: any) => {

      this.productList = response.records;


    }, (error: any) => {
      console.log(error);
    }
    );
  }

  repostOffer(tbid) {
    this.router.navigate(['/addofferEditPage'], { queryParams: { tbid: tbid } });
    console.log(tbid);



  }

  getOfferList() {
    this.http.get('/read_offer',).subscribe((response: any) => {
      this.offerList = response.records;
      console.log(response.records);

    }, (error: any) => {
      console.log(error);
    }
    );
  }

}
