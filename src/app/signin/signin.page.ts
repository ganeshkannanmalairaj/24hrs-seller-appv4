import { Component, OnInit } from '@angular/core';
import { HttpService } from '../shared/http.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { ToastController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.page.html',
  styleUrls: ['./signin.page.scss'],
})
export class SigninPage implements OnInit {

  constructor(private router: Router, private http: HttpService,
    private toastCtrl: ToastController, route: ActivatedRoute) {
    route.params.subscribe(val => {
      // setTimeout(()=>{
      //   if(userdetails != ''){
      //     this.router.navigate(['/tabs'])
      //   }
      // },100)
      console.log(this.loginstatus);

    });
  }

  ngOnInit() {

  }





  loginstatus: any = ""
  signinemailid: any = '';
  signinpassword: any = '';

  signin() {
    const Data = {
      email_id: this.signinemailid,
      password: this.signinpassword
    }

    this.http.post('/seller_login', Data).subscribe((response: any) => {
      console.log(response);
      if (response.success == "true") {
        const obj: any = {
          id: response.tbid,
          storename: response.store_name,
          mobile: response.store_number,
          email: response.email_id,
          token: response.token,
        }
        this.loginstatus = response.seller_status
        console.log(obj);

        const encodeText: any = btoa(JSON.stringify(obj))
        localStorage.setItem("24hrs-user-data", encodeText)
        localStorage.setItem("token", response.token)
        localStorage.setItem("loginstatus", response.seller_status)

        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 1000,
          timerProgressBar: true,
          didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
          }
        })

        Toast.fire({
          icon: 'success',
          title: 'Signed in successfully'
        })
        if (this.loginstatus != "First Login") {
          this.signinemailid = '';
          this.signinpassword = '';
          this.router.navigate(['/sellerpage'])
        } else {
          this.router.navigate(['/tabs'])
        }
      } else {
        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
          }
        })

        Toast.fire({
          icon: 'error',
          title: 'Please enter the valid email & password'
        })
      }
    }, (error: any) => {
      console.log(error);
    }
    );
  }


  signupPage() {
    this.router.navigate(['/signuppage'])
  }

}
