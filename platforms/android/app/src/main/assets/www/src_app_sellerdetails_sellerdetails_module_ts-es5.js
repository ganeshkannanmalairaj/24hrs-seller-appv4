(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (self["webpackChunkseller_app"] = self["webpackChunkseller_app"] || []).push([["src_app_sellerdetails_sellerdetails_module_ts"], {
    /***/
    92779:
    /*!***************************************************************!*\
      !*** ./src/app/sellerdetails/sellerdetails-routing.module.ts ***!
      \***************************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "SellerdetailsPageRoutingModule": function SellerdetailsPageRoutingModule() {
          return (
            /* binding */
            _SellerdetailsPageRoutingModule
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! tslib */
      64762);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      39895);
      /* harmony import */


      var _sellerdetails_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./sellerdetails.page */
      51783);

      var routes = [{
        path: '',
        component: _sellerdetails_page__WEBPACK_IMPORTED_MODULE_0__.SellerdetailsPage
      }];

      var _SellerdetailsPageRoutingModule = function SellerdetailsPageRoutingModule() {
        _classCallCheck(this, SellerdetailsPageRoutingModule);
      };

      _SellerdetailsPageRoutingModule = (0, tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule]
      })], _SellerdetailsPageRoutingModule);
      /***/
    },

    /***/
    76516:
    /*!*******************************************************!*\
      !*** ./src/app/sellerdetails/sellerdetails.module.ts ***!
      \*******************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "SellerdetailsPageModule": function SellerdetailsPageModule() {
          return (
            /* binding */
            _SellerdetailsPageModule
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! tslib */
      64762);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/common */
      38583);
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/forms */
      3679);
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic/angular */
      80476);
      /* harmony import */


      var _sellerdetails_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./sellerdetails-routing.module */
      92779);
      /* harmony import */


      var _sellerdetails_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./sellerdetails.page */
      51783);

      var _SellerdetailsPageModule = function SellerdetailsPageModule() {
        _classCallCheck(this, SellerdetailsPageModule);
      };

      _SellerdetailsPageModule = (0, tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule, _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule, _sellerdetails_routing_module__WEBPACK_IMPORTED_MODULE_0__.SellerdetailsPageRoutingModule],
        declarations: [_sellerdetails_page__WEBPACK_IMPORTED_MODULE_1__.SellerdetailsPage]
      })], _SellerdetailsPageModule);
      /***/
    },

    /***/
    51783:
    /*!*****************************************************!*\
      !*** ./src/app/sellerdetails/sellerdetails.page.ts ***!
      \*****************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "SellerdetailsPage": function SellerdetailsPage() {
          return (
            /* binding */
            _SellerdetailsPage
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! tslib */
      64762);
      /* harmony import */


      var _raw_loader_sellerdetails_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! !raw-loader!./sellerdetails.page.html */
      69762);
      /* harmony import */


      var _sellerdetails_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./sellerdetails.page.scss */
      22912);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var _shared_http_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../shared/http.service */
      28191);
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      39895);

      var _SellerdetailsPage = /*#__PURE__*/function () {
        function SellerdetailsPage(geolocation, router, http) {
          _classCallCheck(this, SellerdetailsPage);

          this.geolocation = geolocation;
          this.router = router;
          this.http = http;
          this.categoryList = [];
        }

        _createClass(SellerdetailsPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            // this.initService()
            this.getCategoryList();
            console.log(this.categoryList);
          }
        }, {
          key: "navigateHome",
          value: function navigateHome() {
            this.router.navigate(['/social-media-details']);
          }
        }, {
          key: "getCategoryList",
          value: function getCategoryList() {
            var _this = this;

            this.http.get('/read_category').subscribe(function (response) {
              console.log(response.records);
              _this.categoryList = response.records;
              console.log(response.records);
              console.log(_this.categoryList);
            }, function (error) {
              console.log(error);
            });
          }
        }]);

        return SellerdetailsPage;
      }();

      _SellerdetailsPage.ctorParameters = function () {
        return [{
          type: Geolocation
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__.Router
        }, {
          type: _shared_http_service__WEBPACK_IMPORTED_MODULE_2__.HttpService
        }];
      };

      _SellerdetailsPage = (0, tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-sellerdetails',
        template: _raw_loader_sellerdetails_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_sellerdetails_page_scss__WEBPACK_IMPORTED_MODULE_1__["default"]]
      })], _SellerdetailsPage);
      /***/
    },

    /***/
    28191:
    /*!****************************************!*\
      !*** ./src/app/shared/http.service.ts ***!
      \****************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "HttpService": function HttpService() {
          return (
            /* binding */
            _HttpService
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! tslib */
      64762);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common/http */
      91841);
      /* harmony import */


      var _environments_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ../../environments/environment */
      92340);

      var _HttpService = /*#__PURE__*/function () {
        function HttpService(http) {
          _classCallCheck(this, HttpService);

          this.http = http;
        }

        _createClass(HttpService, [{
          key: "get",
          value: function get(serviceName) {
            var url = _environments_environment__WEBPACK_IMPORTED_MODULE_0__.environment.baseUrl + serviceName;
            var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__.HttpHeaders();
            var options = {
              headers: headers,
              withCredentials: false
            };
            return this.http.get(url, options);
          }
        }, {
          key: "post",
          value: function post(serviceName, data) {
            var url = _environments_environment__WEBPACK_IMPORTED_MODULE_0__.environment.baseUrl + serviceName;
            var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__.HttpHeaders();
            var options = {
              headers: headers,
              withCredentials: false
            };
            console.log(data);
            return this.http.post(url, JSON.stringify(data), options);
          }
        }]);

        return HttpService;
      }();

      _HttpService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__.HttpClient
        }];
      };

      _HttpService = (0, tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_3__.Injectable)({
        providedIn: 'root'
      })], _HttpService);
      /***/
    },

    /***/
    22912:
    /*!*******************************************************!*\
      !*** ./src/app/sellerdetails/sellerdetails.page.scss ***!
      \*******************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".myproduct-div {\n  position: absolute;\n  background-color: #fff;\n  border: 1px solid #ccc;\n  border-radius: 8px;\n  box-shadow: 2px 2px 2px 2px #ebe6e6;\n  width: 96% !important;\n  margin: 5px;\n}\n\ninput[type=text] {\n  font-size: 11px;\n  margin: 0 5px;\n  margin-top: -12px;\n  margin-left: -20px;\n  width: 90%;\n  box-sizing: border-box;\n  border: none;\n  background-color: #fff;\n  border-bottom: 2px solid #23d5ab;\n}\n\ninput:focus {\n  outline: none !important;\n  border: 1px solid #23d5ab !important;\n  box-shadow: 0 0 2px #23d5ab;\n}\n\n.update-btn {\n  border-radius: 5px !important;\n  background: linear-gradient(to left, #23a6d5 0%, #23d5ab 100%) !important;\n  color: #fff;\n  height: 25px;\n  width: 55%;\n  text-align: center;\n  border: 1px solid #fff;\n  font-size: 12px !important;\n  padding: 3px;\n  margin: 10px !important;\n  margin-left: 35px !important;\n}\n\n.skip-btn {\n  border-radius: 5px !important;\n  background-color: #fff;\n  color: #23d5ab;\n  height: 25px;\n  width: 25%;\n  text-align: center;\n  border: 1px solid #fff;\n  font-size: 13px !important;\n  padding: 3px;\n  margin: 10px !important;\n}\n\nlottie-player {\n  cursor: pointer;\n}\n\nselect {\n  border: 1px solid #fff;\n}\n\nion-content {\n  --background:#F4F7FA !important;\n  overflow: auto;\n}\n\nion-content::-webkit-scrollbar {\n  display: none;\n}\n\n/* Always set the map height explicitly to define the size of the div\n     * element that contains the map. */\n\n#map {\n  height: 100%;\n}\n\n/* Optional: Makes the sample page fill the window. */\n\nhtml,\nbody {\n  height: 100%;\n  margin: 0;\n  padding: 0;\n}\n\n.controls {\n  background-color: #fff;\n  border-radius: 2px;\n  border: 1px solid transparent;\n  box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);\n  box-sizing: border-box;\n  font-family: Roboto;\n  font-size: 15px;\n  font-weight: 300;\n  height: 29px;\n  margin-left: 17px;\n  margin-top: 10px;\n  outline: none;\n  padding: 0 11px 0 13px;\n  text-overflow: ellipsis;\n  width: 400px;\n}\n\n.controls:focus {\n  border-color: #4d90fe;\n}\n\n.title {\n  font-weight: bold;\n}\n\n#infowindow-content {\n  display: none;\n}\n\n#map #infowindow-content {\n  display: inline;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNlbGxlcmRldGFpbHMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usa0JBQUE7RUFDQSxzQkFBQTtFQUNBLHNCQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQ0FBQTtFQUNBLHFCQUFBO0VBQ0EsV0FBQTtBQUNGOztBQUVBO0VBQ0UsZUFBQTtFQUNBLGFBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLHNCQUFBO0VBQ0EsWUFBQTtFQUNBLHNCQUFBO0VBQ0EsZ0NBQUE7QUFDRjs7QUFDQTtFQUNFLHdCQUFBO0VBQ0Esb0NBQUE7RUFDQSwyQkFBQTtBQUVGOztBQUFBO0VBQ0UsNkJBQUE7RUFDQSx5RUFBQTtFQUdBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLGtCQUFBO0VBQ0Esc0JBQUE7RUFDQSwwQkFBQTtFQUNBLFlBQUE7RUFDQSx1QkFBQTtFQUNBLDRCQUFBO0FBQ0Y7O0FBQ0E7RUFDQSw2QkFBQTtFQUNBLHNCQUFBO0VBQ0EsY0FBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0VBQ0Esa0JBQUE7RUFDQSxzQkFBQTtFQUNBLDBCQUFBO0VBQ0EsWUFBQTtFQUNBLHVCQUFBO0FBRUE7O0FBQUE7RUFDRSxlQUFBO0FBR0Y7O0FBQUE7RUFDQSxzQkFBQTtBQUdBOztBQUFBO0VBQ0EsK0JBQUE7RUFDQSxjQUFBO0FBR0E7O0FBRkE7RUFDSSxhQUFBO0FBSUo7O0FBQ0E7dUNBQUE7O0FBRUs7RUFDQyxZQUFBO0FBRU47O0FBQ0kscURBQUE7O0FBQ0E7O0VBRUUsWUFBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0FBRU47O0FBQ0k7RUFDRSxzQkFBQTtFQUNBLGtCQUFBO0VBQ0EsNkJBQUE7RUFDQSx3Q0FBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0VBQ0EsWUFBQTtBQUVOOztBQUNJO0VBQ0UscUJBQUE7QUFFTjs7QUFDSTtFQUNFLGlCQUFBO0FBRU47O0FBQ0k7RUFDRSxhQUFBO0FBRU47O0FBQ0k7RUFDRSxlQUFBO0FBRU4iLCJmaWxlIjoic2VsbGVyZGV0YWlscy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubXlwcm9kdWN0LWRpdntcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xuICBib3JkZXI6MXB4IHNvbGlkICNjY2M7XG4gIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgYm94LXNoYWRvdzogMnB4IDJweCAycHggMnB4ICNlYmU2ZTY7XG4gIHdpZHRoOjk2JSAhaW1wb3J0YW50O1xuICBtYXJnaW46NXB4O1xuICBcbn1cbmlucHV0W3R5cGU9dGV4dF0ge1xuICBmb250LXNpemU6IDExcHg7XG4gIG1hcmdpbjogMCA1cHg7XG4gIG1hcmdpbi10b3A6IC0xMnB4O1xuICBtYXJnaW4tbGVmdDogLTIwcHg7XG4gIHdpZHRoOiA5MCU7XG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gIGJvcmRlcjogbm9uZTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbiAgYm9yZGVyLWJvdHRvbTogMnB4IHNvbGlkICMyM2Q1YWI7O1xufVxuaW5wdXQ6Zm9jdXMge1xuICBvdXRsaW5lOiBub25lICFpbXBvcnRhbnQ7XG4gIGJvcmRlcjoxcHggc29saWQgIzIzZDVhYiAhaW1wb3J0YW50O1xuICBib3gtc2hhZG93OiAwIDAgMnB4ICMyM2Q1YWI7XG59XG4udXBkYXRlLWJ0bntcbiAgYm9yZGVyLXJhZGl1czogNXB4ICFpbXBvcnRhbnQ7XG4gIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCh0byBsZWZ0LCAjMjNhNmQ1XG4gIDAlLCAjMjNkNWFiXG4gIDEwMCUpICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiNmZmY7XG4gIGhlaWdodDoyNXB4O1xuICB3aWR0aDo1NSU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgYm9yZGVyOjFweCBzb2xpZCAjZmZmO1xuICBmb250LXNpemU6IDEycHggIWltcG9ydGFudDtcbiAgcGFkZGluZzogM3B4O1xuICBtYXJnaW46MTBweCAhaW1wb3J0YW50O1xuICBtYXJnaW4tbGVmdDogMzVweCAhaW1wb3J0YW50IDtcbn1cbi5za2lwLWJ0bntcbmJvcmRlci1yYWRpdXM6IDVweCAhaW1wb3J0YW50O1xuYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbmNvbG9yOiMyM2Q1YWI7XG5oZWlnaHQ6MjVweDtcbndpZHRoOjI1JTtcbnRleHQtYWxpZ246IGNlbnRlcjtcbmJvcmRlcjoxcHggc29saWQgI2ZmZjtcbmZvbnQtc2l6ZTogMTNweCAhaW1wb3J0YW50O1xucGFkZGluZzogM3B4O1xubWFyZ2luOjEwcHggIWltcG9ydGFudDtcbn1cbmxvdHRpZS1wbGF5ZXJ7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cblxuc2VsZWN0e1xuYm9yZGVyOiAxcHggc29saWQgI2ZmZjtcblxufVxuaW9uLWNvbnRlbnQge1xuLS1iYWNrZ3JvdW5kOiNGNEY3RkEgICFpbXBvcnRhbnQ7XG5vdmVyZmxvdzogYXV0bztcbiY6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcbiAgICBkaXNwbGF5OiBub25lO1xufVxufVxuXG5cbi8qIEFsd2F5cyBzZXQgdGhlIG1hcCBoZWlnaHQgZXhwbGljaXRseSB0byBkZWZpbmUgdGhlIHNpemUgb2YgdGhlIGRpdlxuICAgICAqIGVsZW1lbnQgdGhhdCBjb250YWlucyB0aGUgbWFwLiAqL1xuICAgICAjbWFwIHtcbiAgICAgIGhlaWdodDogMTAwJTtcbiAgICB9XG4gICAgXG4gICAgLyogT3B0aW9uYWw6IE1ha2VzIHRoZSBzYW1wbGUgcGFnZSBmaWxsIHRoZSB3aW5kb3cuICovXG4gICAgaHRtbCxcbiAgICBib2R5IHtcbiAgICAgIGhlaWdodDogMTAwJTtcbiAgICAgIG1hcmdpbjogMDtcbiAgICAgIHBhZGRpbmc6IDA7XG4gICAgfVxuICAgIFxuICAgIC5jb250cm9scyB7XG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xuICAgICAgYm9yZGVyLXJhZGl1czogMnB4O1xuICAgICAgYm9yZGVyOiAxcHggc29saWQgdHJhbnNwYXJlbnQ7XG4gICAgICBib3gtc2hhZG93OiAwIDJweCA2cHggcmdiYSgwLCAwLCAwLCAwLjMpO1xuICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgICAgIGZvbnQtZmFtaWx5OiBSb2JvdG87XG4gICAgICBmb250LXNpemU6IDE1cHg7XG4gICAgICBmb250LXdlaWdodDogMzAwO1xuICAgICAgaGVpZ2h0OiAyOXB4O1xuICAgICAgbWFyZ2luLWxlZnQ6IDE3cHg7XG4gICAgICBtYXJnaW4tdG9wOiAxMHB4O1xuICAgICAgb3V0bGluZTogbm9uZTtcbiAgICAgIHBhZGRpbmc6IDAgMTFweCAwIDEzcHg7XG4gICAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcbiAgICAgIHdpZHRoOiA0MDBweDtcbiAgICB9XG4gICAgXG4gICAgLmNvbnRyb2xzOmZvY3VzIHtcbiAgICAgIGJvcmRlci1jb2xvcjogIzRkOTBmZTtcbiAgICB9XG4gICAgXG4gICAgLnRpdGxlIHtcbiAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIH1cbiAgICBcbiAgICAjaW5mb3dpbmRvdy1jb250ZW50IHtcbiAgICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgfVxuICAgIFxuICAgICNtYXAgI2luZm93aW5kb3ctY29udGVudCB7XG4gICAgICBkaXNwbGF5OiBpbmxpbmU7XG4gICAgfSJdfQ== */";
      /***/
    },

    /***/
    69762:
    /*!*********************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/sellerdetails/sellerdetails.page.html ***!
      \*********************************************************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content class=\"mt-2\">\n  <div class=\"myproduct-div mt-5 \">\n\n    <div class=\"row\">\n      <div class=\"col-7\">\n        <ion-icon (click)=\"navigateHome()\" style=\"margin:20px;cursor:pointer;color:#23d5ab\" name=\"close\"></ion-icon>\n\n      </div>\n      <div class=\"col-5 ion-text-center\">\n        <!-- <img id=\"logo\" height=\"100px\" width=\"120px\" src=\"assets/logo.jpeg\" alt=\"\"> -->\n        <span class=\"seller\"><b>24hrs</b></span>\n      </div>\n\n    </div>\n    <p style=\"font-size:20px;margin-left:10px;margin-top:10px; color: #5C5C5C;\"> Provide Your Details:</p>\n\n    <div class=\"row\" style=\"padding-left: 10px;\">\n      <div class=\"col-6\">\n        <p class=\"edit-name\">Store logo:</p>\n      </div>\n      <div class=\"col-6\">\n        <lottie-player src=\"https://assets5.lottiefiles.com/packages/lf20_GZxjzF.json\" background=\"transparent\"\n          speed=\"1.5\" style=\"width:40%;margin-left:5px;\" loop autoplay></lottie-player>\n      </div>\n    </div>\n\n    <div class=\"row mt-4\" style=\"padding-left: 10px;\">\n      <div class=\"col-6\">\n        <p class=\"edit-name\">Store Name:</p>\n      </div>\n      <div class=\"col-6\">\n        <input [(ngModel)]=\"storeName\" type=\"text\" id=\"edit-field\" value=\"9876543210\">\n      </div>\n    </div>\n\n    <div class=\"row\" style=\"padding-left: 10px;\">\n      <div class=\"col-6\">\n        <p class=\"edit-name\">Store Address:</p>\n      </div>\n      <div class=\"col-6\">\n        <lottie-player src=\"https://assets6.lottiefiles.com/packages/lf20_CEyvj2.json\" background=\"transparent\"\n          speed=\"1.5\" style=\"width:50%;margin-left:5px;\" loop autoplay></lottie-player>\n      </div>\n    </div>\n\n    <div class=\"row\" style=\"padding-left: 10px;\">\n      <div class=\"col-6 mt-3\">\n        <p class=\"edit-name\">Store category:</p>\n      </div>\n      <div class=\"col-6\">\n        <div class=\"select\" >\n          <select  [(ngModel)]=\"storeCategory\" placeholder=\"category\">\n            <option *ngFor=\"let cat of categoryList\" value=\"{{cat.category}}\">{{cat.category}}</option>\n          </select>\n        </div>\n    </div>\n    </div>\n\n    <div class=\"row\" style=\"padding-left: 10px;\">\n      <div class=\"col-6\">\n        <p class=\"edit-name\">User Name:</p>\n      </div>\n      <div class=\"col-6\">\n        <input  [(ngModel)]=\"userName\" type=\"text\" id=\"edit-field\" value=\"Ravi\">\n      </div>\n    </div>\n\n    <div class=\"row\" style=\"padding-left: 10px;\">\n      <div class=\"col-6\">\n        <p class=\"edit-name\">User Number:</p>\n      </div>\n      <div class=\"col-6\">\n        <input  [(ngModel)]=\"userNumber\" type=\"text\" id=\"edit-field\" value=\"9876543210\">\n      </div>\n    </div>\n\n    <div class=\"row\" style=\"padding-left: 10px;\">\n      <div class=\"col-6\">\n        <p class=\"edit-name\">Id Proff:</p>\n      </div>\n      <div class=\"col-6\">\n        <lottie-player src=\"https://assets5.lottiefiles.com/private_files/lf30_0pkhkmwj.json\" background=\"transparent\"\n          speed=\"1.5\" style=\"width:50%;margin-left:5px;\" loop autoplay></lottie-player>\n      </div>\n    </div>\n\n    <div class=\"row\" style=\"padding-left: 10px;\">\n      <div class=\"col-6\">\n        <p class=\"edit-name\">Address Proff:</p>\n      </div>\n      <div class=\"col-6\">\n        <lottie-player src=\"https://assets5.lottiefiles.com/packages/lf20_jxgqawba.json\" background=\"transparent\"\n          speed=\"1.5\" style=\"width:50%;margin-left:5px;\" loop autoplay></lottie-player>\n      </div>\n    </div>\n\n    <div class=\"row \">\n       <div class=\"col-7\">\n        <button (click)=\"navigateHome()\" type=\"button\" class=\"btn btn-success btn-sm skip-btn\">Skip</button>\n       </div>\n       <div class=\"col-5\">\n        <button (click)=\"navigateHome()\" type=\"button\" class=\"btn btn-success btn-sm update-btn\">Next</button>\n      </div>\n    </div>\n\n    <div id=\"map\"></div>\n\n  \n</div>\n  </div>\n</ion-content>";
      /***/
    }
  }]);
})();
//# sourceMappingURL=src_app_sellerdetails_sellerdetails_module_ts-es5.js.map