import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { HttpService } from '../shared/http.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-addoffer-edit',
  templateUrl: './addoffer-edit.page.html',
  styleUrls: ['./addoffer-edit.page.scss'],
})
export class AddofferEditPage implements OnInit {

  constructor(private http: HttpService, private router: Router, private route: ActivatedRoute) {
    route.params.subscribe(val => {
      this.getCategoryList()
      this.getProductList()
      this.getsubCategoryList()
      this.gettBid()
      this.readOneOffer()
      

    });
  }

  ngOnInit() {
  }
  tbid_value: any;
  categoryList: any;
  productList: any;
  subcategoryList = [];
  Category: any = '';
  subcategory: any = '';
  productName: any = '';
  offer: any = '';
  totalCost: any = '';
  offerPrice: any = '';
  offerTime: any = '';
  description: any = '';
  otheroffer: any = "";
  backAddoffer() {
    this.router.navigate(['/tabs/tab4'])
  }

  getCategoryList() {

    this.http.get('/read_category',).subscribe((response: any) => {

      this.categoryList = response.records
      console.log(response.records);

    }, (error: any) => {
      console.log(error);
    }
    );
  }

  gettBid() {
    this.route.queryParams.subscribe(params => {
      console.log(params.tbid);
      this.tbid_value = params.tbid;
    }
    );
  }


  readOneOffer() {
    this.http.get('/read_one_offer?o=' + this.tbid_value).subscribe((response: any) => {
      this.description = response.records.description;
      this.Category = response.records.category;
      this.subcategory = response.records.subcategory;
      this.productName = response.records.product_name;
      this.offer = response.records.offer;
      this.otheroffer = response.records.other_offer;
      this.totalCost = response.records.total_cost;
      this.offerPrice = response.records.offer_price;
      this.offerTime = response.records.offer_time;
    }, (error: any) => {
      console.log(error);
    }
    )
  }



  offerDelete() {
    const obj = {
      tbid: this.tbid_value
    }
    this.http.post('/delete_offer', obj).subscribe((response: any) => {
      console.log(response);
      if (response.success == "true") {
        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 1000,
          timerProgressBar: true,
          didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
          }
        })

        Toast.fire({
          icon: 'success',
          title: 'Deleted successfully'
        })
      }

    }, (error: any) => {
      console.log(error);
    }
    )
  }

  getsubCategoryList() {

    this.http.get('/read_subcategory',).subscribe((response: any) => {

      this.subcategoryList = response.records
      console.log(response.records);

    }, (error: any) => {
      console.log(error);
    }
    );
  }

  getProductList() {
    this.http.get('/read_product',).subscribe((response: any) => {

      this.productList = response.records
      console.log(response.records);

    }, (error: any) => {
      console.log(error);
    }
    );
  }

  addoffer() {

    const productData = {
      tbid: 1,
      category: this.Category,
      subcategory: this.subcategory,
      product: this.productName,
      description: this.description,
      offer: this.offer,
      other_offer: this.otheroffer,
      total_cost: this.totalCost,
      offer_price: this.offerPrice,
      offer_time: this.offerTime
    }


    this.http.post('/update_offer', productData).subscribe((response: any) => {
      console.log(response);
      if (response.success == "true") {
        console.log(response);
        console.log(productData);
      }
    }, (error: any) => {
      console.log(error);
    }
    );

  }
}
