(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (self["webpackChunkseller_app"] = self["webpackChunkseller_app"] || []).push([["src_app_splashscreen_splashscreen_module_ts"], {
    /***/
    53860:
    /*!*************************************************************!*\
      !*** ./src/app/splashscreen/splashscreen-routing.module.ts ***!
      \*************************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "SplashscreenPageRoutingModule": function SplashscreenPageRoutingModule() {
          return (
            /* binding */
            _SplashscreenPageRoutingModule
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! tslib */
      64762);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      39895);
      /* harmony import */


      var _splashscreen_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./splashscreen.page */
      39836);

      var routes = [{
        path: '',
        component: _splashscreen_page__WEBPACK_IMPORTED_MODULE_0__.SplashscreenPage
      }];

      var _SplashscreenPageRoutingModule = function SplashscreenPageRoutingModule() {
        _classCallCheck(this, SplashscreenPageRoutingModule);
      };

      _SplashscreenPageRoutingModule = (0, tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule]
      })], _SplashscreenPageRoutingModule);
      /***/
    },

    /***/
    85481:
    /*!*****************************************************!*\
      !*** ./src/app/splashscreen/splashscreen.module.ts ***!
      \*****************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "SplashscreenPageModule": function SplashscreenPageModule() {
          return (
            /* binding */
            _SplashscreenPageModule
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! tslib */
      64762);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/common */
      38583);
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/forms */
      3679);
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic/angular */
      80476);
      /* harmony import */


      var _splashscreen_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./splashscreen-routing.module */
      53860);
      /* harmony import */


      var _splashscreen_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./splashscreen.page */
      39836);

      var _SplashscreenPageModule = function SplashscreenPageModule() {
        _classCallCheck(this, SplashscreenPageModule);
      };

      _SplashscreenPageModule = (0, tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule, _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule, _splashscreen_routing_module__WEBPACK_IMPORTED_MODULE_0__.SplashscreenPageRoutingModule],
        declarations: [_splashscreen_page__WEBPACK_IMPORTED_MODULE_1__.SplashscreenPage]
      })], _SplashscreenPageModule);
      /***/
    },

    /***/
    39836:
    /*!***************************************************!*\
      !*** ./src/app/splashscreen/splashscreen.page.ts ***!
      \***************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "SplashscreenPage": function SplashscreenPage() {
          return (
            /* binding */
            _SplashscreenPage
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! tslib */
      64762);
      /* harmony import */


      var _raw_loader_splashscreen_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! !raw-loader!./splashscreen.page.html */
      61608);
      /* harmony import */


      var _splashscreen_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./splashscreen.page.scss */
      5733);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      39895);

      var _SplashscreenPage = /*#__PURE__*/function () {
        function SplashscreenPage(router, route) {
          var _this = this;

          _classCallCheck(this, SplashscreenPage);

          this.router = router;
          route.params.subscribe(function (val) {
            console.log(localStorage.getItem("24hrs-user-data"));
            var userdetails = localStorage.getItem("24hrs-user-data");
            console.log(userdetails);
            setTimeout(function () {
              if (userdetails) {
                _this.router.navigate(['/tabs']);
              } else {
                _this.router.navigate(['/signinpage']);
              }
            }, 1500);
          });
        }

        _createClass(SplashscreenPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return SplashscreenPage;
      }();

      _SplashscreenPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__.Router
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__.ActivatedRoute
        }];
      };

      _SplashscreenPage = (0, tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_4__.Component)({
        selector: 'app-splashscreen',
        template: _raw_loader_splashscreen_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_splashscreen_page_scss__WEBPACK_IMPORTED_MODULE_1__["default"]]
      })], _SplashscreenPage);
      /***/
    },

    /***/
    5733:
    /*!*****************************************************!*\
      !*** ./src/app/splashscreen/splashscreen.page.scss ***!
      \*****************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".element {\n  margin-top: 60% !important;\n  height: 200px;\n  width: 200px;\n  margin: 0 auto;\n  background-color: #fff;\n  box-shadow: 0px 5px 17px -7px black;\n  border-radius: 50%;\n  -webkit-animation-name: stretch;\n          animation-name: stretch;\n  -webkit-animation-duration: 0.7s;\n          animation-duration: 0.7s;\n  -webkit-animation-timing-function: ease-out;\n          animation-timing-function: ease-out;\n  -webkit-animation-delay: 0;\n          animation-delay: 0;\n  -webkit-animation-direction: alternate;\n          animation-direction: alternate;\n  -webkit-animation-fill-mode: none;\n          animation-fill-mode: none;\n  -webkit-animation-play-state: running;\n          animation-play-state: running;\n}\n\n@-webkit-keyframes stretch {\n  0% {\n    transform: scale(0.7);\n    background-color: #fff;\n    border-radius: 100%;\n  }\n  50% {\n    background-color: #fff;\n  }\n  100% {\n    transform: scale(1);\n    background-color: #fff;\n  }\n}\n\n@keyframes stretch {\n  0% {\n    transform: scale(0.7);\n    background-color: #fff;\n    border-radius: 100%;\n  }\n  50% {\n    background-color: #fff;\n  }\n  100% {\n    transform: scale(1);\n    background-color: #fff;\n  }\n}\n\nion-content {\n  --background: linear-gradient(to left, #23a6d5\n  0%, #23d5ab\n  100%) !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNwbGFzaHNjcmVlbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSwwQkFBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0VBQ0EsY0FBQTtFQUNBLHNCQUFBO0VBQ0EsbUNBQUE7RUFDQSxrQkFBQTtFQUNBLCtCQUFBO1VBQUEsdUJBQUE7RUFDQSxnQ0FBQTtVQUFBLHdCQUFBO0VBQ0EsMkNBQUE7VUFBQSxtQ0FBQTtFQUNBLDBCQUFBO1VBQUEsa0JBQUE7RUFDQSxzQ0FBQTtVQUFBLDhCQUFBO0VBRUEsaUNBQUE7VUFBQSx5QkFBQTtFQUNBLHFDQUFBO1VBQUEsNkJBQUE7QUFBRjs7QUFHQTtFQUNFO0lBQ0UscUJBQUE7SUFDQSxzQkFBQTtJQUNBLG1CQUFBO0VBQUY7RUFFQTtJQUNFLHNCQUFBO0VBQUY7RUFFQTtJQUNFLG1CQUFBO0lBQ0Esc0JBQUE7RUFBRjtBQUNGOztBQVpBO0VBQ0U7SUFDRSxxQkFBQTtJQUNBLHNCQUFBO0lBQ0EsbUJBQUE7RUFBRjtFQUVBO0lBQ0Usc0JBQUE7RUFBRjtFQUVBO0lBQ0UsbUJBQUE7SUFDQSxzQkFBQTtFQUFGO0FBQ0Y7O0FBRUE7RUFDRTs7a0JBQUE7QUFFRiIsImZpbGUiOiJzcGxhc2hzY3JlZW4ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmVsZW1lbnQge1xuICBtYXJnaW4tdG9wOiA2MCUgIWltcG9ydGFudDtcbiAgaGVpZ2h0OiAyMDBweDtcbiAgd2lkdGg6IDIwMHB4O1xuICBtYXJnaW46IDAgYXV0bztcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbiAgYm94LXNoYWRvdzogMHB4IDVweCAxN3B4IC03cHggcmdiYSgwLCAwLCAwLCA1LjUpO1xuICBib3JkZXItcmFkaXVzOjUwJTtcbiAgYW5pbWF0aW9uLW5hbWU6IHN0cmV0Y2g7XG4gIGFuaW1hdGlvbi1kdXJhdGlvbjogLjdzOyBcbiAgYW5pbWF0aW9uLXRpbWluZy1mdW5jdGlvbjogZWFzZS1vdXQ7IFxuICBhbmltYXRpb24tZGVsYXk6IDA7XG4gIGFuaW1hdGlvbi1kaXJlY3Rpb246IGFsdGVybmF0ZTtcbiAgLy8gYW5pbWF0aW9uLWl0ZXJhdGlvbi1jb3VudDogaW5maW5pdGU7XG4gIGFuaW1hdGlvbi1maWxsLW1vZGU6IG5vbmU7XG4gIGFuaW1hdGlvbi1wbGF5LXN0YXRlOiBydW5uaW5nO1xufVxuXG5Aa2V5ZnJhbWVzIHN0cmV0Y2gge1xuICAwJSB7XG4gICAgdHJhbnNmb3JtOiBzY2FsZSguNyk7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbiAgICBib3JkZXItcmFkaXVzOiAxMDAlO1xuICB9XG4gIDUwJSB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbiAgfVxuICAxMDAlIHtcbiAgICB0cmFuc2Zvcm06IHNjYWxlKDEuMCk7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbiAgfVxufVxuaW9uLWNvbnRlbnR7XG4gIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIGxlZnQsICMyM2E2ZDVcbiAgMCUsICMyM2Q1YWJcbiAgMTAwJSkgIWltcG9ydGFudDtcbn1cbiJdfQ== */";
      /***/
    },

    /***/
    61608:
    /*!*******************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/splashscreen/splashscreen.page.html ***!
      \*******************************************************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n<ion-content>\n  <div class=\"element\">\n    <img src=\"assets/logo.jpeg\" alt=\"\">\n  </div>\n  \n</ion-content>\n";
      /***/
    }
  }]);
})();
//# sourceMappingURL=src_app_splashscreen_splashscreen_module_ts-es5.js.map