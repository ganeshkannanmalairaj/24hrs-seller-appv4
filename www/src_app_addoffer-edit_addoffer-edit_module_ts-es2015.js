(self["webpackChunkseller_app"] = self["webpackChunkseller_app"] || []).push([["src_app_addoffer-edit_addoffer-edit_module_ts"],{

/***/ 52444:
/*!***************************************************************!*\
  !*** ./src/app/addoffer-edit/addoffer-edit-routing.module.ts ***!
  \***************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AddofferEditPageRoutingModule": function() { return /* binding */ AddofferEditPageRoutingModule; }
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var _addoffer_edit_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./addoffer-edit.page */ 65776);




const routes = [
    {
        path: '',
        component: _addoffer_edit_page__WEBPACK_IMPORTED_MODULE_0__.AddofferEditPage
    }
];
let AddofferEditPageRoutingModule = class AddofferEditPageRoutingModule {
};
AddofferEditPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], AddofferEditPageRoutingModule);



/***/ }),

/***/ 74329:
/*!*******************************************************!*\
  !*** ./src/app/addoffer-edit/addoffer-edit.module.ts ***!
  \*******************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AddofferEditPageModule": function() { return /* binding */ AddofferEditPageModule; }
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _addoffer_edit_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./addoffer-edit-routing.module */ 52444);
/* harmony import */ var _addoffer_edit_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./addoffer-edit.page */ 65776);







let AddofferEditPageModule = class AddofferEditPageModule {
};
AddofferEditPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _addoffer_edit_routing_module__WEBPACK_IMPORTED_MODULE_0__.AddofferEditPageRoutingModule
        ],
        declarations: [_addoffer_edit_page__WEBPACK_IMPORTED_MODULE_1__.AddofferEditPage]
    })
], AddofferEditPageModule);



/***/ }),

/***/ 65776:
/*!*****************************************************!*\
  !*** ./src/app/addoffer-edit/addoffer-edit.page.ts ***!
  \*****************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AddofferEditPage": function() { return /* binding */ AddofferEditPage; }
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _raw_loader_addoffer_edit_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./addoffer-edit.page.html */ 12529);
/* harmony import */ var _addoffer_edit_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./addoffer-edit.page.scss */ 97446);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var _shared_http_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../shared/http.service */ 28191);







let AddofferEditPage = class AddofferEditPage {
    constructor(http, router, route) {
        this.http = http;
        this.router = router;
        this.route = route;
        route.params.subscribe(val => {
            this.getCategoryList();
            this.getProductList();
        });
    }
    ngOnInit() {
    }
    backAddoffer() {
        this.router.navigate(['/tabs/tab4']);
    }
    getCategoryList() {
        this.http.get('/read_category').subscribe((response) => {
            this.categoryList = response.records;
            console.log(response.records);
        }, (error) => {
            console.log(error);
        });
    }
    getProductList() {
        this.http.get('/read_product').subscribe((response) => {
            this.productList = response.records;
            console.log(response.records);
        }, (error) => {
            console.log(error);
        });
    }
};
AddofferEditPage.ctorParameters = () => [
    { type: _shared_http_service__WEBPACK_IMPORTED_MODULE_2__.HttpService },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__.Router },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__.ActivatedRoute }
];
AddofferEditPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-addoffer-edit',
        template: _raw_loader_addoffer_edit_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_addoffer_edit_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], AddofferEditPage);



/***/ }),

/***/ 28191:
/*!****************************************!*\
  !*** ./src/app/shared/http.service.ts ***!
  \****************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HttpService": function() { return /* binding */ HttpService; }
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ 91841);
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../environments/environment */ 92340);




let HttpService = class HttpService {
    constructor(http) {
        this.http = http;
    }
    get(serviceName) {
        const userdetails = JSON.parse(atob(localStorage.getItem("24hrs-user-data")));
        const url = _environments_environment__WEBPACK_IMPORTED_MODULE_0__.environment.baseUrl + serviceName;
        const header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__.HttpHeaders()
            // .set("Access-Control-Allow-Origin", "*")
            // .set("Access-Control-Allow-Methods", "GET,POST")
            // .set('Accept','application/json')
            // .set('Content-Type','application/json')
            // .set("Access-Control-Allow-Headers", "Token, Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With")
            .set("Token", userdetails.token);
        //   let httpOptions = {
        //     headers: new HttpHeaders({
        //       "Access-Control-Allow-Origin": "*",
        //       "Access-Control-Allow-Methods": "*",
        //       "Access-Control-Allow-Headers":"Token, Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With",
        //       'Authorization':userdetails.token,
        //       "Content-Type": "application/json"
        //     })
        // };
        const options = { headers: header, withCredentials: true };
        return this.http.get(url, { headers: header });
    }
    post(serviceName, data) {
        const token = ((localStorage.getItem("token")));
        const url = _environments_environment__WEBPACK_IMPORTED_MODULE_0__.environment.baseUrl + serviceName;
        if (serviceName == '/user_login' || serviceName == '/user_register') {
            const headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__.HttpHeaders();
            const options = { headers: headers, withCredentials: false };
            return this.http.post(url, JSON.stringify(data), { headers: headers });
        }
        else {
            const headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__.HttpHeaders().set("Token", token);
            const options = { headers: headers, withCredentials: true };
            return this.http.post(url, JSON.stringify(data), { headers: headers });
        }
    }
};
HttpService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__.HttpClient }
];
HttpService = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Injectable)({
        providedIn: 'root'
    })
], HttpService);



/***/ }),

/***/ 97446:
/*!*******************************************************!*\
  !*** ./src/app/addoffer-edit/addoffer-edit.page.scss ***!
  \*******************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".card {\n  background-color: #fff;\n  border: 1px solid #bfbfbf;\n  border-radius: 5px;\n  box-shadow: 2px 2px 2px 2px #ccc !important;\n  width: 96% !important;\n  margin: 5px;\n}\n\n#offer:focus {\n  outline: none !important;\n  border: 1px solid #fff;\n  box-shadow: 0 0 2px #e9f3ff;\n}\n\n.confirm-btn {\n  border-radius: 5px !important;\n  width: 50%;\n  height: 35px;\n  background: linear-gradient(to left, #23a6d5 0%, #23d5ab 100%);\n  color: #fff;\n  border: 1px solid #fff;\n  margin: 5px;\n}\n\n#kg-dropdwon {\n  position: relative;\n  display: flex;\n  width: 100%;\n  height: 3em;\n  border-radius: 0.25em;\n  overflow: hidden;\n}\n\ninput[type=text] {\n  font-size: 15px;\n  margin: 0 5px;\n  width: 90%;\n  box-sizing: border-box;\n  border: none;\n  background-color: #fff;\n  border-bottom: 2px solid #23d5ab;\n}\n\ninput[type=text]:focus {\n  border-color: #23d5ab !important;\n  box-shadow: 0 0 8px 0 #23d5ab !important;\n}\n\n#edit {\n  cursor: pointer;\n}\n\n.myproduct-div {\n  position: absolute;\n  background-color: #fff;\n  border: 1px solid #ccc;\n  border-radius: 8px;\n  box-shadow: 2px 2px 2px 2px #ebe6e6;\n  width: 96% !important;\n  margin: 5px;\n}\n\n.select {\n  width: 95% !important;\n  text-align: center !important;\n}\n\n.repost-btn {\n  background-color: #23d5ab;\n  color: #fff;\n  text-align: center;\n}\n\n.delete-btn {\n  background: linear-gradient(to right, #ff80a0 0%, #eb154b 100%);\n  color: #fff;\n  text-align: center;\n  margin-bottom: 30px;\n}\n\n.update-btn {\n  background: linear-gradient(to left, #23a6d5 0%, #23d5ab 100%) !important;\n  color: #fff;\n  text-align: center;\n  margin-bottom: 30px;\n}\n\nion-content {\n  background-color: #F4F7FA !important;\n  --offset-bottom: auto!important;\n  --overflow: hidden;\n  overflow: auto;\n}\n\nion-content::-webkit-scrollbar {\n  display: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFkZG9mZmVyLWVkaXQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0Usc0JBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsMkNBQUE7RUFDQSxxQkFBQTtFQUNBLFdBQUE7QUFBRjs7QUFHQTtFQUNBLHdCQUFBO0VBQ0Esc0JBQUE7RUFDQSwyQkFBQTtBQUFBOztBQUdBO0VBQ0EsNkJBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtFQUNBLDhEQUFBO0VBR0EsV0FBQTtFQUNBLHNCQUFBO0VBQ0EsV0FBQTtBQUZBOztBQUtBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7RUFDQSxxQkFBQTtFQUNBLGdCQUFBO0FBRkE7O0FBSUE7RUFDQSxlQUFBO0VBQ0EsYUFBQTtFQUNBLFVBQUE7RUFDQSxzQkFBQTtFQUNBLFlBQUE7RUFDQSxzQkFBQTtFQUNBLGdDQUFBO0FBREE7O0FBUUE7RUFDQSxnQ0FBQTtFQUNBLHdDQUFBO0FBTEE7O0FBT0E7RUFDQSxlQUFBO0FBSkE7O0FBTUE7RUFDQSxrQkFBQTtFQUNBLHNCQUFBO0VBQ0Esc0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1DQUFBO0VBQ0EscUJBQUE7RUFDQSxXQUFBO0FBSEE7O0FBTUE7RUFDQSxxQkFBQTtFQUNBLDZCQUFBO0FBSEE7O0FBTUE7RUFDQSx5QkFBQTtFQUNBLFdBQUE7RUFFQSxrQkFBQTtBQUpBOztBQVFBO0VBQ0UsK0RBQUE7RUFHQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQVBGOztBQVNFO0VBQ0EseUVBQUE7RUFHQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQVJGOztBQVVBO0VBQ0Esb0NBQUE7RUFLQSwrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtBQVhBOztBQVlBO0VBQ0ksYUFBQTtBQVZKIiwiZmlsZSI6ImFkZG9mZmVyLWVkaXQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXG4uY2FyZHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbiAgYm9yZGVyOjFweCBzb2xpZCAjYmZiZmJmO1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIGJveC1zaGFkb3c6IDJweCAycHggMnB4IDJweCAjY2NjICFpbXBvcnRhbnQ7XG4gIHdpZHRoOjk2JSAhaW1wb3J0YW50O1xuICBtYXJnaW46NXB4O1xufVxuXG4jb2ZmZXI6Zm9jdXMge1xub3V0bGluZTogbm9uZSAhaW1wb3J0YW50O1xuYm9yZGVyOjFweCBzb2xpZCAjZmZmO1xuYm94LXNoYWRvdzogMCAwIDJweCAjZTlmM2ZmO1xufVxuXG4uY29uZmlybS1idG57XG5ib3JkZXItcmFkaXVzOiA1cHggIWltcG9ydGFudDtcbndpZHRoOiA1MCU7XG5oZWlnaHQ6MzVweDtcbmJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCh0byBsZWZ0LCAjMjNhNmQ1XG4gIDAlLCAjMjNkNWFiXG4gIDEwMCUpO1xuY29sb3I6I2ZmZjtcbmJvcmRlcjoxcHggc29saWQgI2ZmZjtcbm1hcmdpbjo1cHg7XG59XG5cbiNrZy1kcm9wZHdvbiB7XG5wb3NpdGlvbjogcmVsYXRpdmU7XG5kaXNwbGF5OiBmbGV4O1xud2lkdGg6IDEwMCU7XG5oZWlnaHQ6IDNlbTtcbmJvcmRlci1yYWRpdXM6IC4yNWVtO1xub3ZlcmZsb3c6IGhpZGRlbjtcbn1cbmlucHV0W3R5cGU9dGV4dF0ge1xuZm9udC1zaXplOiAxNXB4O1xubWFyZ2luOiAwIDVweDtcbndpZHRoOiA5MCU7XG5ib3gtc2l6aW5nOiBib3JkZXItYm94O1xuYm9yZGVyOiBub25lO1xuYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbmJvcmRlci1ib3R0b206IDJweCBzb2xpZCAjMjNkNWFiO1xufVxuLy8gaW5wdXQ6Zm9jdXMge1xuLy8gICBvdXRsaW5lOiBub25lICFpbXBvcnRhbnQ7XG4vLyAgIGJvcmRlcjoxcHggc29saWQgI2U5ZjNmZiAhaW1wb3J0YW50O1xuLy8gICBib3gtc2hhZG93OiAwIDAgMnB4ICM4YjhkOTE7XG4vLyB9XG5pbnB1dFt0eXBlPVwidGV4dFwiXTpmb2N1cyB7XG5ib3JkZXItY29sb3I6ICMyM2Q1YWIgIWltcG9ydGFudDtcbmJveC1zaGFkb3c6IDAgMCA4cHggMCAjMjNkNWFiICFpbXBvcnRhbnQ7XG59XG4jZWRpdHtcbmN1cnNvcjogcG9pbnRlcjtcbn1cbi5teXByb2R1Y3QtZGl2e1xucG9zaXRpb246IGFic29sdXRlO1xuYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbmJvcmRlcjoxcHggc29saWQgI2NjYztcbmJvcmRlci1yYWRpdXM6IDhweDtcbmJveC1zaGFkb3c6IDJweCAycHggMnB4IDJweCAjZWJlNmU2O1xud2lkdGg6OTYlICFpbXBvcnRhbnQ7XG5tYXJnaW46NXB4O1xuXG59XG4uc2VsZWN0e1xud2lkdGg6OTUlICFpbXBvcnRhbnQ7XG50ZXh0LWFsaWduOiBjZW50ZXIgIWltcG9ydGFudDtcbn1cblxuLnJlcG9zdC1idG57XG5iYWNrZ3JvdW5kLWNvbG9yOiAjMjNkNWFiO1xuY29sb3I6I2ZmZjtcbi8vIG1hcmdpbi10b3A6IC01cHg7XG50ZXh0LWFsaWduOiBjZW50ZXI7XG5cbn1cblxuLmRlbGV0ZS1idG4ge1xuICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICNmZjgwYTBcbiAgMCUsICNlYjE1NGJcbiAgMTAwJSk7XG4gIGNvbG9yOiNmZmY7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLWJvdHRvbTogMzBweDtcbiAgfVxuICAudXBkYXRlLWJ0bntcbiAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIGxlZnQsICMyM2E2ZDVcbiAgMCUsICMyM2Q1YWJcbiAgMTAwJSkgIWltcG9ydGFudDtcbiAgY29sb3I6I2ZmZjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tYm90dG9tOiAzMHB4O1xuICB9XG5pb24tY29udGVudCB7XG5iYWNrZ3JvdW5kLWNvbG9yOiNGNEY3RkEgICFpbXBvcnRhbnQ7XG4vLyBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQodG8gYm90dG9tLCBkYXJrdHVycXVvaXNlXG4vLyAwJSwgI2QxZjJlN1xuLy8gMTAwJSkgIWltcG9ydGFudDtcblxuLS1vZmZzZXQtYm90dG9tOiBhdXRvIWltcG9ydGFudDtcbi0tb3ZlcmZsb3c6IGhpZGRlbjtcbm92ZXJmbG93OiBhdXRvO1xuJjo6LXdlYmtpdC1zY3JvbGxiYXIge1xuICAgIGRpc3BsYXk6IG5vbmU7XG59XG59Il19 */");

/***/ }),

/***/ 12529:
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/addoffer-edit/addoffer-edit.page.html ***!
  \*********************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header [translucent]=\"true\">\n  <ion-toolbar class=\"new-background-color\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button style=\"margin-top:15px;color:#fff\" autoHide=\"false\"></ion-menu-button>\n    </ion-buttons>\n\n    <div class=\"row\">\n      <div class=\"col-5\">\n        <h5 style=\"color:#fff;margin-top:30px\">24HRS</h5>\n      </div>\n      <div class=\"col-7\" style=\"text-align: right;width: 90%;margin-top: -40px;\">\n        <svg style=\"margin:5px;text-align:right\" width=\"16\" height=\"20\" viewBox=\"0 0 16 20\" fill=\"none\"\n          xmlns=\"http://www.w3.org/2000/svg\">\n          <path fill-rule=\"evenodd\" clip-rule=\"evenodd\"\n            d=\"M8.0002 0C7.11654 0 6.4002 0.716344 6.4002 1.6V1.80156C3.63963 2.51189 1.5998 5.01775 1.5998 8.00003V13.6H2C0.895431 13.6 0 14.4955 0 15.6V15.8C0 16.3523 0.447715 16.8 1 16.8H15C15.5523 16.8 16 16.3523 16 15.8V15.6C16 14.4955 15.1046 13.6 14 13.6H14.3998V8.00003C14.3998 5.01803 12.3604 2.51237 9.60019 1.80176V1.6C9.60019 0.716344 8.88385 0 8.0002 0ZM5.5998 17.6C5.5998 18.9255 6.67432 20 7.9998 20C9.32529 20 10.3998 18.9255 10.3998 17.6H5.5998Z\"\n            fill=\"white\" />\n        </svg>\n      </div>\n    </div>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"container\">\n  <div  class= \"signin-div mb-2\" style=\"margin-top: 20px;\">\n    <div class=\"container\">\n    <div class=\"row\">\n      <div class=\"col-12 ion-text-left \" style=\"margin-top: 20px;\">\n\n        <svg (click)=\"backAddoffer()\" style=\"margin-top:-8px;cursor:pointer;margin:5px;\" xmlns=\"http://www.w3.org/2000/svg\"\n          width=\"22\" height=\"20\" fill=\"#23d5ab\" class=\"bi bi-arrow-left\" viewBox=\"0 0 16 16\">\n          <path fill-rule=\"evenodd\"\n            d=\"M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z\" />\n        </svg>\n      </div>\n      <div class=\"col-12\">\n        <h5 class=\"name  ion-text-center\" style=\"color:#676767;\"><b>Edit Your Offer</b></h5>\n        <p class=\"name mt-3 ion-text-left\" style=\"color:#404040;\">Category :</p>\n      </div>\n      <div class=\"col-12\">\n        <div class=\"select\" id=\"kg-dropdwon\">\n          <select>\n            <option *ngFor=\"let category of categoryList\" value=\"{{category.category}}\">{{category.category}}</option>\n          </select>\n        </div>\n      </div>\n\n      <div class=\"col-12\">\n        <p class=\"name mt-3 ion-text-left\" style=\"color:#404040;\">Sub Category :</p>\n      </div>\n      <div class=\"col-12\">\n        <div class=\"select\" id=\"kg-dropdwon\">\n          <select>\n            <option *ngFor=\"let category of categoryList\" value=\"{{category.category}}\">{{category.category}}</option>\n          </select>\n        </div>\n      </div>\n    </div>\n\n    <div class=\"row\">\n      <div class=\"col-12 ion-text-left\">\n        <p class=\"name mt-2\" style=\"color:#404040;\">Product:</p>\n      </div>\n      <div class=\"col-12\">\n        <div class=\"select\" id=\"kg-dropdwon\">\n          <select>\n            <option *ngFor=\"let product of productList\" value=\"{{product.product_name}}\">{{product.product_name}}\n            </option>\n          </select>\n        </div>\n      </div>\n    </div>\n\n    <div class=\"row mt-4\" >\n      <div class=\"col-6\">\n        <p class=\"edit-name ion-text-left\" style=\"color:#404040;\" >Offer:</p>\n      </div>\n      <div class=\"col-6\">\n        <input [(ngModel)]=\"cost\" type=\"text\"  placeholder=\"10%\" id=\"offer\" >\n      </div>\n    </div>\n\n    <div class=\"row\" >\n      <div class=\"col-6\">\n        <p class=\"edit-name ion-text-left\" style=\"color:#404040;\" >Total Cost :</p>\n      </div>\n      <div class=\"col-6\">\n        <input [(ngModel)]=\"cost\" type=\"text\"  placeholder=\"130\" id=\"offer\" >\n      </div>\n    </div>\n\n    <div class=\"row\" >\n      <div class=\"col-6\">\n        <p class=\"edit-name ion-text-left\" style=\"color:#404040;\" >Offer Price :</p>\n      </div>\n      <div class=\"col-6\">\n        <input [(ngModel)]=\"cost\" type=\"text\"  placeholder=\"117\" id=\"offer\" >\n      </div>\n    </div>\n\n    <div class=\"row\" >\n      <div class=\"col-6\">\n        <p class=\"edit-name ion-text-left\" style=\"color:#404040;\" >Offer Time :</p>\n      </div>\n      <div class=\"col-6\">\n        <input [(ngModel)]=\"cost\" type=\"text\"  placeholder=\"2:00:00\" id=\"offer\" >\n      </div>\n    </div>\n\n    <div class=\"form-group mt-3 ion-text-center\">\n      <label for=\"exampleFormControlTextarea1\" style=\"color:#404040;\"> Description </label>\n      <textarea style=\"background-color: #fff;margin-top: 5px;\" class=\"form-control\" id=\"exampleFormControlTextarea1\"\n        rows=\"3\"></textarea>\n    </div>\n\n    <div class=\"row\">\n      <div class=\"col-6 ion-text-center\">\n        <button (click)=\"addToProduct()\" class=\"btn btn-sm delete-btn mt-2\">Delete</button>\n      </div>\n      <div class=\"col-6 ion-text-center\">\n        <button class=\"btn btn-sm update-btn mt-2\">Update</button>\n      </div>\n    </div>\n  </div>\n</div>\n</div>\n</ion-content>");

/***/ })

}]);
//# sourceMappingURL=src_app_addoffer-edit_addoffer-edit_module_ts-es2015.js.map