import { Component, OnInit} from '@angular/core';
import { HttpService } from '../shared/http.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-sellerdetails',
  templateUrl: './sellerdetails.page.html',
  styleUrls: ['./sellerdetails.page.scss'],
})
export class SellerdetailsPage implements OnInit {


 

  constructor(private router: Router, private http: HttpService, route: ActivatedRoute) {
    route.params.subscribe(val => {
      this.getCategoryList()
    });
   }

  ngOnInit() {
  }

  categoryList: any = [];




  navigateHome() {
    this.router.navigate(['/social-media-details'])
  }

  getCategoryList() {
    this.http.get('/read_category',).subscribe((response: any) => {
      console.log(response.records);
      this.categoryList = response.records
      console.log(response.records);
      console.log(this.categoryList);

    }, (error: any) => {
      console.log(error);
    }
    );
  }

 

}
