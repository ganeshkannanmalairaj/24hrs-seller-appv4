(self["webpackChunkseller_app"] = self["webpackChunkseller_app"] || []).push([["src_app_addoffer-edit_addoffer-edit_module_ts"],{

/***/ 52444:
/*!***************************************************************!*\
  !*** ./src/app/addoffer-edit/addoffer-edit-routing.module.ts ***!
  \***************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AddofferEditPageRoutingModule": function() { return /* binding */ AddofferEditPageRoutingModule; }
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var _addoffer_edit_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./addoffer-edit.page */ 65776);




const routes = [
    {
        path: '',
        component: _addoffer_edit_page__WEBPACK_IMPORTED_MODULE_0__.AddofferEditPage
    }
];
let AddofferEditPageRoutingModule = class AddofferEditPageRoutingModule {
};
AddofferEditPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], AddofferEditPageRoutingModule);



/***/ }),

/***/ 74329:
/*!*******************************************************!*\
  !*** ./src/app/addoffer-edit/addoffer-edit.module.ts ***!
  \*******************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AddofferEditPageModule": function() { return /* binding */ AddofferEditPageModule; }
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _addoffer_edit_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./addoffer-edit-routing.module */ 52444);
/* harmony import */ var _addoffer_edit_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./addoffer-edit.page */ 65776);







let AddofferEditPageModule = class AddofferEditPageModule {
};
AddofferEditPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _addoffer_edit_routing_module__WEBPACK_IMPORTED_MODULE_0__.AddofferEditPageRoutingModule
        ],
        declarations: [_addoffer_edit_page__WEBPACK_IMPORTED_MODULE_1__.AddofferEditPage]
    })
], AddofferEditPageModule);



/***/ }),

/***/ 65776:
/*!*****************************************************!*\
  !*** ./src/app/addoffer-edit/addoffer-edit.page.ts ***!
  \*****************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AddofferEditPage": function() { return /* binding */ AddofferEditPage; }
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _raw_loader_addoffer_edit_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./addoffer-edit.page.html */ 12529);
/* harmony import */ var _addoffer_edit_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./addoffer-edit.page.scss */ 97446);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var _shared_http_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../shared/http.service */ 28191);







let AddofferEditPage = class AddofferEditPage {
    constructor(http, router, route) {
        this.http = http;
        this.router = router;
        this.route = route;
        route.params.subscribe(val => {
            this.getCategoryList();
            this.getProductList();
        });
    }
    ngOnInit() {
    }
    backAddoffer() {
        this.router.navigate(['/tabs/tab4']);
    }
    getCategoryList() {
        this.http.get('/read_category').subscribe((response) => {
            this.categoryList = response.records;
            console.log(response.records);
        }, (error) => {
            console.log(error);
        });
    }
    getProductList() {
        this.http.get('/read_product').subscribe((response) => {
            this.productList = response.records;
            console.log(response.records);
        }, (error) => {
            console.log(error);
        });
    }
};
AddofferEditPage.ctorParameters = () => [
    { type: _shared_http_service__WEBPACK_IMPORTED_MODULE_2__.HttpService },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__.Router },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__.ActivatedRoute }
];
AddofferEditPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-addoffer-edit',
        template: _raw_loader_addoffer_edit_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_addoffer_edit_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], AddofferEditPage);



/***/ }),

/***/ 28191:
/*!****************************************!*\
  !*** ./src/app/shared/http.service.ts ***!
  \****************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HttpService": function() { return /* binding */ HttpService; }
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ 91841);
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../environments/environment */ 92340);




let HttpService = class HttpService {
    constructor(http) {
        this.http = http;
    }
    get(serviceName) {
        const url = _environments_environment__WEBPACK_IMPORTED_MODULE_0__.environment.baseUrl + serviceName;
        const headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__.HttpHeaders();
        const options = { headers: headers, withCredentials: false };
        return this.http.get(url, options);
    }
    post(serviceName, data) {
        const url = _environments_environment__WEBPACK_IMPORTED_MODULE_0__.environment.baseUrl + serviceName;
        const headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__.HttpHeaders();
        const options = { headers: headers, withCredentials: false };
        console.log(data);
        return this.http.post(url, JSON.stringify(data), options);
    }
};
HttpService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__.HttpClient }
];
HttpService = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Injectable)({
        providedIn: 'root'
    })
], HttpService);



/***/ }),

/***/ 97446:
/*!*******************************************************!*\
  !*** ./src/app/addoffer-edit/addoffer-edit.page.scss ***!
  \*******************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".card {\n  background-color: #fff;\n  border: 1px solid #bfbfbf;\n  border-radius: 5px;\n  box-shadow: 2px 2px 2px 2px #ccc !important;\n  width: 96% !important;\n  margin: 5px;\n}\n\nimg {\n  margin: 12px 10px;\n  height: 65px;\n  width: 75x;\n}\n\n.offer-btn {\n  margin: 25px !important;\n  background-color: #FF6000 !important;\n  border: 1px solid #fff;\n  border-radius: 5px;\n  color: #fff;\n}\n\n.product-name {\n  margin-top: -10px !important;\n  text-align: center;\n}\n\n.badge {\n  font-size: 10px !important;\n  margin-left: 20px !important;\n  color: #50C7C8;\n}\n\n#offer:focus {\n  outline: none !important;\n  border: 1px solid #fff;\n  box-shadow: 0 0 2px #e9f3ff;\n}\n\n.confirm-btn {\n  border-radius: 5px !important;\n  width: 50%;\n  height: 35px;\n  background-color: #FF6000;\n  color: #fff;\n  border: 1px solid #fff;\n  margin: 5px;\n}\n\n#card {\n  background-color: #fff;\n  border-radius: 5px;\n  box-shadow: 2px 2px 2px 2px #ccc;\n  margin-bottom: 5px;\n  border: 1px solid #ebe6e6;\n}\n\n#blurred {\n  -moz-filter: blur(2px);\n  -o-filter: blur(2px);\n  -ms-filter: blur(2px);\n  filter: blur(2px);\n}\n\n#kg-dropdwon {\n  position: relative;\n  display: flex;\n  width: 100%;\n  height: 3em;\n  border-radius: 0.25em;\n  overflow: hidden;\n}\n\ninput[type=text] {\n  font-size: 15px;\n  margin: 0 5px;\n  width: 90%;\n  box-sizing: border-box;\n  border: none;\n  background-color: #fff;\n  border-bottom: 2px solid #FF6000;\n}\n\ninput:focus {\n  outline: none !important;\n  border: 1px solid #e9f3ff !important;\n  box-shadow: 0 0 2px #8b8d91;\n}\n\n#edit {\n  cursor: pointer;\n}\n\n.myproduct-div {\n  position: absolute;\n  background-color: #fff;\n  border: 1px solid #ccc;\n  border-radius: 8px;\n  box-shadow: 2px 2px 2px 2px #ebe6e6;\n  width: 96% !important;\n  margin: 5px;\n}\n\n.delete-btn {\n  background-color: #f7391b !important;\n  color: #fff;\n  text-align: center;\n}\n\n.update-btn {\n  background-color: #29C17E;\n  color: #fff;\n  text-align: center;\n}\n\n.btn {\n  margin: 10px 0 !important;\n}\n\n.select {\n  width: 95% !important;\n  text-align: center !important;\n}\n\nion-content {\n  background-color: #F4F7FA !important;\n  --offset-bottom: auto!important;\n  --overflow: hidden;\n  overflow: auto;\n}\n\nion-content::-webkit-scrollbar {\n  display: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFkZG9mZmVyLWVkaXQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usc0JBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsMkNBQUE7RUFDQSxxQkFBQTtFQUNBLFdBQUE7QUFDRjs7QUFDQTtFQUNFLGlCQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7QUFFRjs7QUFBQTtFQUNFLHVCQUFBO0VBQ0Esb0NBQUE7RUFDQSxzQkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtBQUdGOztBQURBO0VBQ0UsNEJBQUE7RUFDQSxrQkFBQTtBQUlGOztBQUZBO0VBQ0UsMEJBQUE7RUFDQSw0QkFBQTtFQUNBLGNBQUE7QUFLRjs7QUFDQTtFQUNBLHdCQUFBO0VBQ0Esc0JBQUE7RUFDQSwyQkFBQTtBQUVBOztBQUNBO0VBQ0EsNkJBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtFQUNBLHlCQUFBO0VBQ0EsV0FBQTtFQUNBLHNCQUFBO0VBQ0EsV0FBQTtBQUVBOztBQUFBO0VBQ0Esc0JBQUE7RUFDQSxrQkFBQTtFQUNBLGdDQUFBO0VBQ0Esa0JBQUE7RUFDQSx5QkFBQTtBQUdBOztBQURBO0VBRUEsc0JBQUE7RUFDQSxvQkFBQTtFQUNBLHFCQUFBO0VBQ0EsaUJBQUE7QUFJQTs7QUFGQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0EscUJBQUE7RUFDQSxnQkFBQTtBQUtBOztBQUZBO0VBQ0EsZUFBQTtFQUNBLGFBQUE7RUFDQSxVQUFBO0VBQ0Esc0JBQUE7RUFDQSxZQUFBO0VBQ0Esc0JBQUE7RUFDQSxnQ0FBQTtBQUtBOztBQUhBO0VBQ0Esd0JBQUE7RUFDQSxvQ0FBQTtFQUNBLDJCQUFBO0FBTUE7O0FBSEE7RUFDQSxlQUFBO0FBTUE7O0FBSkE7RUFDQSxrQkFBQTtFQUNBLHNCQUFBO0VBQ0Esc0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1DQUFBO0VBQ0EscUJBQUE7RUFDQSxXQUFBO0FBT0E7O0FBSEE7RUFDRSxvQ0FBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtBQU1GOztBQUpBO0VBQ0kseUJBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7QUFPSjs7QUFMQTtFQUNFLHlCQUFBO0FBUUY7O0FBTEE7RUFDRSxxQkFBQTtFQUNBLDZCQUFBO0FBUUY7O0FBTkE7RUFDQSxvQ0FBQTtFQUtBLCtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0FBS0E7O0FBSkE7RUFDSSxhQUFBO0FBTUoiLCJmaWxlIjoiYWRkb2ZmZXItZWRpdC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY2FyZHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbiAgYm9yZGVyOjFweCBzb2xpZCAjYmZiZmJmO1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIGJveC1zaGFkb3c6IDJweCAycHggMnB4IDJweCAjY2NjICFpbXBvcnRhbnQ7XG4gIHdpZHRoOjk2JSAhaW1wb3J0YW50O1xuICBtYXJnaW46NXB4O1xufVxuaW1ne1xuICBtYXJnaW46MTJweCAxMHB4O1xuICBoZWlnaHQ6NjVweDtcbiAgd2lkdGg6NzV4XG59XG4ub2ZmZXItYnRue1xuICBtYXJnaW46MjVweCAhaW1wb3J0YW50O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjRkY2MDAwICFpbXBvcnRhbnQ7XG4gIGJvcmRlcjoxcHggc29saWQgI2ZmZjtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBjb2xvcjogI2ZmZjtcbn1cbi5wcm9kdWN0LW5hbWV7XG4gIG1hcmdpbi10b3A6IC0xMHB4ICFpbXBvcnRhbnQ7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5iYWRnZXtcbiAgZm9udC1zaXplOiAxMHB4ICFpbXBvcnRhbnQ7XG4gIG1hcmdpbi1sZWZ0OjIwcHggIWltcG9ydGFudDtcbiAgY29sb3I6IzUwQzdDOFxuICA7XG59XG5cblxuXG4jb2ZmZXI6Zm9jdXMge1xub3V0bGluZTogbm9uZSAhaW1wb3J0YW50O1xuYm9yZGVyOjFweCBzb2xpZCAjZmZmO1xuYm94LXNoYWRvdzogMCAwIDJweCAjZTlmM2ZmO1xufVxuXG4uY29uZmlybS1idG57XG5ib3JkZXItcmFkaXVzOiA1cHggIWltcG9ydGFudDtcbndpZHRoOiA1MCU7XG5oZWlnaHQ6MzVweDtcbmJhY2tncm91bmQtY29sb3I6ICNGRjYwMDA7XG5jb2xvcjojZmZmO1xuYm9yZGVyOjFweCBzb2xpZCAjZmZmO1xubWFyZ2luOjVweDtcbn1cbiNjYXJke1xuYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbmJvcmRlci1yYWRpdXM6IDVweDtcbmJveC1zaGFkb3c6IDJweCAycHggMnB4IDJweCAjY2NjO1xubWFyZ2luLWJvdHRvbTogNXB4O1xuYm9yZGVyOjFweCBzb2xpZCAjZWJlNmU2O1xufVxuI2JsdXJyZWQge1xuLXdlYmtpdC1maWx0ZXI6IGJsdXIoMnB4KTtcbi1tb3otZmlsdGVyOiBibHVyKDJweCk7XG4tby1maWx0ZXI6IGJsdXIoMnB4KTtcbi1tcy1maWx0ZXI6IGJsdXIoMnB4KTtcbmZpbHRlcjogYmx1cigycHgpOyAgICBcbn1cbiNrZy1kcm9wZHdvbiB7XG5wb3NpdGlvbjogcmVsYXRpdmU7XG5kaXNwbGF5OiBmbGV4O1xud2lkdGg6IDEwMCU7XG5oZWlnaHQ6IDNlbTtcbmJvcmRlci1yYWRpdXM6IC4yNWVtO1xub3ZlcmZsb3c6IGhpZGRlbjtcblxufVxuaW5wdXRbdHlwZT10ZXh0XSB7XG5mb250LXNpemU6IDE1cHg7XG5tYXJnaW46IDAgNXB4O1xud2lkdGg6IDkwJTtcbmJveC1zaXppbmc6IGJvcmRlci1ib3g7XG5ib3JkZXI6IG5vbmU7XG5iYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xuYm9yZGVyLWJvdHRvbTogMnB4IHNvbGlkICNGRjYwMDA7XG59XG5pbnB1dDpmb2N1cyB7XG5vdXRsaW5lOiBub25lICFpbXBvcnRhbnQ7XG5ib3JkZXI6MXB4IHNvbGlkICNlOWYzZmYgIWltcG9ydGFudDtcbmJveC1zaGFkb3c6IDAgMCAycHggIzhiOGQ5MTtcbn1cblxuI2VkaXR7XG5jdXJzb3I6IHBvaW50ZXI7XG59XG4ubXlwcm9kdWN0LWRpdntcbnBvc2l0aW9uOiBhYnNvbHV0ZTtcbmJhY2tncm91bmQtY29sb3I6ICNmZmY7XG5ib3JkZXI6MXB4IHNvbGlkICNjY2M7XG5ib3JkZXItcmFkaXVzOiA4cHg7XG5ib3gtc2hhZG93OiAycHggMnB4IDJweCAycHggI2ViZTZlNjtcbndpZHRoOjk2JSAhaW1wb3J0YW50O1xubWFyZ2luOjVweDtcblxufVxuXG4uZGVsZXRlLWJ0biB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmNzM5MWIgIWltcG9ydGFudDtcbiAgY29sb3I6I2ZmZjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLnVwZGF0ZS1idG57XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzI5QzE3RTtcbiAgICBjb2xvcjojZmZmO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5idG57XG4gIG1hcmdpbjoxMHB4IDAgIWltcG9ydGFudDtcbn1cblxuLnNlbGVjdHtcbiAgd2lkdGg6OTUlICFpbXBvcnRhbnQ7XG4gIHRleHQtYWxpZ246IGNlbnRlciAhaW1wb3J0YW50O1xufVxuaW9uLWNvbnRlbnQge1xuYmFja2dyb3VuZC1jb2xvcjojRjRGN0ZBICAhaW1wb3J0YW50O1xuLy8gYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIGJvdHRvbSwgZGFya3R1cnF1b2lzZVxuLy8gMCUsICNkMWYyZTdcbi8vIDEwMCUpICFpbXBvcnRhbnQ7XG5cbi0tb2Zmc2V0LWJvdHRvbTogYXV0byFpbXBvcnRhbnQ7XG4tLW92ZXJmbG93OiBoaWRkZW47XG5vdmVyZmxvdzogYXV0bztcbiY6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcbiAgICBkaXNwbGF5OiBub25lO1xufVxufSJdfQ== */");

/***/ }),

/***/ 12529:
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/addoffer-edit/addoffer-edit.page.html ***!
  \*********************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header [translucent]=\"true\">\n  <ion-toolbar class=\"new-background-color\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button style=\"margin-top:15px;color:#fff\" autoHide=\"false\"></ion-menu-button>\n    </ion-buttons>\n\n    <div class=\"row\">\n      <div class=\"col-5\">\n        <h5 style=\"color:#fff;margin-top:30px\">24HRS</h5>\n      </div>\n      <div class=\"col-7\" style=\"text-align: right;width: 90%;margin-top: -40px;\">\n        <svg style=\"margin:5px;text-align:right\" width=\"16\" height=\"20\" viewBox=\"0 0 16 20\" fill=\"none\"\n          xmlns=\"http://www.w3.org/2000/svg\">\n          <path fill-rule=\"evenodd\" clip-rule=\"evenodd\"\n            d=\"M8.0002 0C7.11654 0 6.4002 0.716344 6.4002 1.6V1.80156C3.63963 2.51189 1.5998 5.01775 1.5998 8.00003V13.6H2C0.895431 13.6 0 14.4955 0 15.6V15.8C0 16.3523 0.447715 16.8 1 16.8H15C15.5523 16.8 16 16.3523 16 15.8V15.6C16 14.4955 15.1046 13.6 14 13.6H14.3998V8.00003C14.3998 5.01803 12.3604 2.51237 9.60019 1.80176V1.6C9.60019 0.716344 8.88385 0 8.0002 0ZM5.5998 17.6C5.5998 18.9255 6.67432 20 7.9998 20C9.32529 20 10.3998 18.9255 10.3998 17.6H5.5998Z\"\n            fill=\"white\" />\n        </svg>\n      </div>\n    </div>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"container\">\n  <div  class= \"signin-div\" style=\"margin-top: 20px;\">\n    <div class=\"container\">\n    <div class=\"row\">\n      <div class=\"col-12 ion-text-left \" style=\"margin-top: 20px;\">\n\n        <svg (click)=\"backAddoffer()\" style=\"margin-top:-8px;cursor:pointer;margin:5px;\" xmlns=\"http://www.w3.org/2000/svg\"\n          width=\"22\" height=\"20\" fill=\"#FF6000\" class=\"bi bi-arrow-left\" viewBox=\"0 0 16 16\">\n          <path fill-rule=\"evenodd\"\n            d=\"M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z\" />\n        </svg>\n      </div>\n      <div class=\"col-12\">\n        <h5 class=\"name  ion-text-center\" style=\"color:#676767;\"><b>Edit Your Offer</b></h5>\n        <p class=\"name mt-3 ion-text-left\" style=\"color:#404040;\">Category :</p>\n      </div>\n      <div class=\"col-12\">\n        <div class=\"select\" id=\"kg-dropdwon\">\n          <select>\n            <option *ngFor=\"let category of categoryList\" value=\"{{category.category}}\">{{category.category}}</option>\n          </select>\n        </div>\n      </div>\n\n      <div class=\"col-12\">\n        <p class=\"name mt-3 ion-text-left\" style=\"color:#404040;\">Sub Category :</p>\n      </div>\n      <div class=\"col-12\">\n        <div class=\"select\" id=\"kg-dropdwon\">\n          <select>\n            <option *ngFor=\"let category of categoryList\" value=\"{{category.category}}\">{{category.category}}</option>\n          </select>\n        </div>\n      </div>\n    </div>\n\n    <div class=\"row\">\n      <div class=\"col-12 ion-text-left\">\n        <p class=\"name mt-2\" style=\"color:#404040;\">Product:</p>\n      </div>\n      <div class=\"col-12\">\n        <div class=\"select\" id=\"kg-dropdwon\">\n          <select>\n            <option *ngFor=\"let product of productList\" value=\"{{product.product_name}}\">{{product.product_name}}\n            </option>\n          </select>\n        </div>\n      </div>\n    </div>\n\n    <div class=\"row mt-4\">\n      <div class=\"col-6\">\n        <p class=\"edit-name ion-text-left\" style=\"color:#404040;\">Offer:</p>\n      </div>\n      <div class=\"col-6\">\n        <input [(ngModel)]=\"cost\" type=\"text\" placeholder=\"100\" id=\"offer\">\n      </div>\n    </div>\n\n    <div class=\"row\">\n      <div class=\"col-6\">\n        <p class=\"edit-name ion-text-left\" style=\"color:#404040;\">Total Cost :</p>\n      </div>\n      <div class=\"col-6\">\n        <input [(ngModel)]=\"cost\" type=\"text\" placeholder=\"100\" id=\"offer\">\n      </div>\n    </div>\n\n    <div class=\"row\">\n      <div class=\"col-6\">\n        <p class=\"edit-name ion-text-left\" style=\"color:#404040;\">Offer Price :</p>\n      </div>\n      <div class=\"col-6\">\n        <input type=\"text\" id=\"edit-field\" value=\"115\">\n      </div>\n    </div>\n\n    <div class=\"row\">\n      <div class=\"col-6\">\n        <p class=\"edit-name ion-text-left\" style=\"color:#404040;\">Offer Time :</p>\n      </div>\n      <div class=\"col-6\">\n        <input type=\"text\" id=\"edit-field\" value=\"2:00:00\">\n      </div>\n    </div>\n\n    <div class=\"form-group mt-3 ion-text-center\">\n      <label for=\"exampleFormControlTextarea1\" style=\"color:#404040;\"> Description </label>\n      <textarea style=\"background-color: #fff;margin-top: 5px;\" class=\"form-control\" id=\"exampleFormControlTextarea1\"\n        rows=\"3\"></textarea>\n    </div>\n\n    <div class=\"row\">\n      <div class=\"col-6 ion-text-center\">\n        <button (click)=\"addToProduct()\" class=\"btn btn-sm delete-btn mt-2\">Delete</button>\n      </div>\n      <div class=\"col-6 ion-text-center\">\n        <button class=\"btn btn-sm update-btn\">Update</button>\n      </div>\n    </div>\n  </div>\n</div>\n</div>\n</ion-content>");

/***/ })

}]);
//# sourceMappingURL=src_app_addoffer-edit_addoffer-edit_module_ts-es2015.js.map