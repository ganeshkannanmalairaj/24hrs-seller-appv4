import { Component } from '@angular/core';
import { Ng2GoogleChartsModule } from 'ng2-google-charts';
import * as HighCharts from 'highcharts';
import { Router } from '@angular/router';
import { HttpService } from '../shared/http.service';
@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  constructor(private http: HttpService, private router: Router,) { }
  ionViewDidEnter() {
    this.plotSimplePieChart();
    this.displaydetail();
  }


  displaydetails = [];
  showdate:"";
  showtime:"";

  displaydetail() {
    this.http.get('/read_offer').subscribe((response: any) => {
      console.log(response);
      this.displaydetails = response.records;

    
      


    }, (error: any) => {
      console.log(error);
    }
    );
  }

  plotSimplePieChart() {

    let myChart = HighCharts.chart('dynamicSpline', {
      chart: {
        plotBackgroundColor: "#fff",
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie',


      },
      credits: {
        enabled: false
      },
      title: {
        text: 'Your Promotions',
        style: {
          color: '#5C5C5C',

        }
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',

          dataLabels: {
            enabled: false,
            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
            style: {
              color: 'black',

            }
          }
        }
      },
      series: [{
        name: 'Promotion Status',
        colorByPoint: true,
        type: undefined,
        data: [{
          name: 'Viewd',
          y: 61.41,
          color: '#29C17E',
          width: 1500,
          height: 2000,
          sliced: true,
          selected: true
        }, {
          name: 'Not viewd',
          color: '#FEC501',
          y: 11.84
        }, {
          name: 'Expired',
          color: '#5BC0EB',
          y: 10.85
        }]
      }]
    });

  }
  NavigatetoNotification() {
    this.router.navigate(['/notification'])
  }
}
