import { Component, OnInit } from '@angular/core';
import { HttpService } from '../shared/http.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { ToastController } from '@ionic/angular';
@Component({
  selector: 'app-social-media-details',
  templateUrl: './social-media-details.page.html',
  styleUrls: ['./social-media-details.page.scss'],
})
export class SocialMediaDetailsPage implements OnInit {

  constructor(private router: Router, private http: HttpService, route: ActivatedRoute) {
    route.params.subscribe(val => {

    });
  }

  ngOnInit() {
    console.log(this.parsedObj);
  }
  userdetails: any = atob(localStorage.getItem("24hrs-user-data"));

  parsedObj: any = JSON.parse(this.userdetails);




  
  website: any;
  instagram: any;
  whatsapp: any;
  youtube: any;
  facebook: any;
  mobilenumber: any



  sellerContact() {

    const contactData = {
      tbid: this.parsedObj.id,
      website: this.website,
      whatsapp: this.whatsapp,
      instagram: this.instagram,
      facebook: this.facebook,
      youtube: this.youtube,
      contact_number: this.mobilenumber
    }
    console.log(contactData);

    this.http.post('/seller_contact', contactData).subscribe((response: any) => {
      if (response.success == "true") {
        console.log(response);

        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 1000,
          timerProgressBar: true,
          didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
          }
        })

        Toast.fire({
          icon: 'success',
          title: 'updated successfully'
        })
        this.router.navigate(['/tabs'])
      } else {

      }

    }, (error: any) => {
      console.log(error);
    }
    );
  }

  navigateHome() {
    this.router.navigate(['/tabs'])
  }

}
