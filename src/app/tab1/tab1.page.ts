import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { MenuController, NavController, Platform, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HttpService } from '../shared/http.service';
import Swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';

import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  Marker,
  GoogleMapsAnimation,
  MyLocation
} from '@ionic-native/google-maps';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit, OnDestroy, AfterViewInit {
  map: GoogleMap;
  address: string;
  backButtonSubscription;
  constructor(private platform: Platform, public toastCtrl: ToastController, private http: HttpService, private router: Router, private menu: MenuController, route: ActivatedRoute, public navCtrl: NavController) {
    route.params.subscribe(val => {
      this.userdetails = (JSON.parse(window.atob(localStorage.getItem("24hrs-user-data"))));
      this.offersCount()

      this.list()
    });
  }

  ngOnInit() {

    this.platform.ready();
    // this.loadMap();

  }

  NavigatetoNotification() {
    this.router.navigate(['/notification'])
  }
  userdetails: any = atob(localStorage.getItem("24hrs-user-data"));
  parsedObj: any = JSON.parse(this.userdetails);

  listOfCat: any = [];
  listOfProduct: any = [];
  encodeText: any;
  offerCount: any = 0;
  offerObj = {
    card: "false",
    form: "true"
  }


  loadMap() {
    this.map = GoogleMaps.create('map_canvas', {
      // camera: {
      //   target: {
      //     lat: 43.0741704,
      //     lng: -89.3809802
      //   },
      //   zoom: 18,
      //   tilt: 30
      // }
    });
    this.goToMyLocation();
  }


  goToMyLocation() {
    this.map.clear();
    const myLatLng = { lat: -25.363, lng: 131.044 };
    // Get the location of you
    this.map.getMyLocation().then((location: MyLocation) => {
      console.log(JSON.stringify(location, null, 2));

      // Move the map camera to the location with animation
      this.map.animateCamera({
        target: myLatLng,
        zoom: 17,
        duration: 5000
      });

      //add a marker
      let marker: Marker = this.map.addMarkerSync({
        title: '@ionic-native/google-maps plugin!',
        snippet: 'This plugin is awesome!',
        position: myLatLng,
        animation: GoogleMapsAnimation.BOUNCE
      });

      //show the infoWindow
      marker.showInfoWindow();

      //If clicked it, display the alert
      marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
        this.showToast('clicked!');
      });

      this.map.on(GoogleMapsEvent.MAP_READY).subscribe(
        (data) => {
          console.log("Click MAP", data);
        }
      );
    })
      .catch(err => {
        //this.loading.dismiss();
        this.showToast(err.error_message);
      });
  }

  async showToast(message: string) {
    let toast = await this.toastCtrl.create({
      message: message,
      duration: 2000,
      position: 'middle'
    });
    toast.present();
  }

  myproducts() {
    this.router.navigate(['/myproducts'])
  }



  list() {
    this.http.get('/read_category',).subscribe((response: any) => {
      this.listOfCat = response.records;
    }, (error: any) => {
      console.log(error);
    }
    );
  }

  offers() {
    this.router.navigate(['/tabs/tab4'])
  }

  offersCount() {
    this.http.get('/read_offer',).subscribe((response: any) => {
      this.offerCount = response.records.length;
    }, (error: any) => {
      console.log(error);
    }
    );
  }

  views() {
    this.router.navigate(['/tabs/tab2'])
  }


  ngAfterViewInit() {
    this.backButtonSubscription = this.platform.backButton.subscribe(() => {
      navigator['app'].exitApp();
    });
  }
  ngOnDestroy() { };
}
