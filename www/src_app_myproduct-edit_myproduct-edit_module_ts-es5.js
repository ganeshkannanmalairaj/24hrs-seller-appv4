(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (self["webpackChunkseller_app"] = self["webpackChunkseller_app"] || []).push([["src_app_myproduct-edit_myproduct-edit_module_ts"], {
    /***/
    57676:
    /*!*****************************************************************!*\
      !*** ./src/app/myproduct-edit/myproduct-edit-routing.module.ts ***!
      \*****************************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "MyproductEditPageRoutingModule": function MyproductEditPageRoutingModule() {
          return (
            /* binding */
            _MyproductEditPageRoutingModule
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! tslib */
      64762);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      39895);
      /* harmony import */


      var _myproduct_edit_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./myproduct-edit.page */
      67591);

      var routes = [{
        path: '',
        component: _myproduct_edit_page__WEBPACK_IMPORTED_MODULE_0__.MyproductEditPage
      }];

      var _MyproductEditPageRoutingModule = function MyproductEditPageRoutingModule() {
        _classCallCheck(this, MyproductEditPageRoutingModule);
      };

      _MyproductEditPageRoutingModule = (0, tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule]
      })], _MyproductEditPageRoutingModule);
      /***/
    },

    /***/
    86093:
    /*!*********************************************************!*\
      !*** ./src/app/myproduct-edit/myproduct-edit.module.ts ***!
      \*********************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "MyproductEditPageModule": function MyproductEditPageModule() {
          return (
            /* binding */
            _MyproductEditPageModule
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! tslib */
      64762);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/common */
      38583);
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/forms */
      3679);
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic/angular */
      80476);
      /* harmony import */


      var _myproduct_edit_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./myproduct-edit-routing.module */
      57676);
      /* harmony import */


      var _myproduct_edit_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./myproduct-edit.page */
      67591);

      var _MyproductEditPageModule = function MyproductEditPageModule() {
        _classCallCheck(this, MyproductEditPageModule);
      };

      _MyproductEditPageModule = (0, tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule, _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule, _myproduct_edit_routing_module__WEBPACK_IMPORTED_MODULE_0__.MyproductEditPageRoutingModule],
        declarations: [_myproduct_edit_page__WEBPACK_IMPORTED_MODULE_1__.MyproductEditPage]
      })], _MyproductEditPageModule);
      /***/
    },

    /***/
    67591:
    /*!*******************************************************!*\
      !*** ./src/app/myproduct-edit/myproduct-edit.page.ts ***!
      \*******************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "MyproductEditPage": function MyproductEditPage() {
          return (
            /* binding */
            _MyproductEditPage
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! tslib */
      64762);
      /* harmony import */


      var _raw_loader_myproduct_edit_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! !raw-loader!./myproduct-edit.page.html */
      47009);
      /* harmony import */


      var _myproduct_edit_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./myproduct-edit.page.scss */
      52677);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      39895);
      /* harmony import */


      var _shared_http_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../shared/http.service */
      28191);

      var _MyproductEditPage = /*#__PURE__*/function () {
        function MyproductEditPage(http, router, route) {
          var _this = this;

          _classCallCheck(this, MyproductEditPage);

          this.http = http;
          this.router = router;
          this.route = route;
          this.date = new Date().toISOString();
          this.Category = '';
          this.subcategory = '';
          this.productname = '';
          this.description = '';
          this.cost = '';
          this.categoryName = '';
          this.subcategoryName = '';
          this.categoryList = [];
          this.subcategoryList = [];
          this.PopupModel = false;
          this.subcategoryPopupModel = false;
          route.params.subscribe(function (val) {
            _this.getCategoryList();

            _this.getSubcategoryList();
          });
        }

        _createClass(MyproductEditPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "backToprivious",
          value: function backToprivious() {
            this.PopupModel = false;
            this.Category = '';
          }
        }, {
          key: "ScBackToprivious",
          value: function ScBackToprivious() {
            this.subcategoryPopupModel = false;
            this.subcategory = '';
          }
        }, {
          key: "addproduct",
          value: function addproduct() {
            this.router.navigate(['/myproducts']);
          }
        }, {
          key: "createCategory",
          value: function createCategory() {
            var _this2 = this;

            this.PopupModel = false;
            var catData = {
              category_name: this.categoryName,
              created_at: this.date
            };
            this.http.post('/create_category', catData).subscribe(function (response) {
              console.log(response);

              if (response.success == "true") {
                _this2.Category = '';
                _this2.categoryName = '';

                _this2.getCategoryList();
              }
            }, function (error) {
              console.log(error);
            });
          }
        }, {
          key: "createSubcategory",
          value: function createSubcategory() {
            var _this3 = this;

            this.subcategoryPopupModel = false;
            var subcatData = {
              category: this.Category,
              subcategory_name: this.subcategoryName
            };
            this.http.post('/create_subcategory', subcatData).subscribe(function (response) {
              console.log(response);

              if (response.success == "true") {
                console.log("test");
                _this3.Category = '';
                _this3.subcategory = '';
                _this3.subcategoryName = '';

                _this3.getSubcategoryList();
              }
            }, function (error) {
              console.log(error);
            });
          }
        }, {
          key: "getCategoryList",
          value: function getCategoryList() {
            var _this4 = this;

            this.http.get('/read_category').subscribe(function (response) {
              console.log(response.records);
              _this4.categoryList = response.records;
              console.log(response.records);
              console.log(_this4.categoryList);
            }, function (error) {
              console.log(error);
            });
          }
        }, {
          key: "getSubcategoryList",
          value: function getSubcategoryList() {
            var _this5 = this;

            this.http.get('/read_subcategory').subscribe(function (response) {
              console.log(response.records);
              _this5.subcategoryList = response.records;
              console.log(response.records);
              console.log(_this5.subcategoryList);
            }, function (error) {
              console.log(error);
            });
          }
        }, {
          key: "popupModelOpen",
          value: function popupModelOpen() {
            if (this.Category == "1") {
              this.PopupModel = true;
            }
          }
        }, {
          key: "subcategoryPopupModelOpen",
          value: function subcategoryPopupModelOpen() {
            if (this.subcategory == "2") {
              this.subcategoryPopupModel = true;
            }
          }
        }, {
          key: "addToProduct",
          value: function addToProduct() {
            var _this6 = this;

            var productData = {
              category: this.Category,
              subcategory: this.subcategory,
              product_name: this.productname,
              description: this.description,
              cost: this.cost
            };
            this.http.post('/update_product', productData).subscribe(function (response) {
              console.log(response);

              if (response.success == "true") {
                // const Toast = Swal.mixin({
                //   toast: true,
                //   position: 'top-end',
                //   showConfirmButton: false,
                //   timer: 1000,
                //   timerProgressBar: true,
                //   didOpen: (toast) => {
                //     toast.addEventListener('mouseenter', Swal.stopTimer)
                //     toast.addEventListener('mouseleave', Swal.resumeTimer)
                //   }
                // })
                // Toast.fire({
                //   icon: 'success',
                //   title: 'Signed in successfully'
                // })
                _this6.addproduct();
              }
            }, function (error) {
              console.log(error);
            });
          }
        }, {
          key: "backToMyproducts",
          value: function backToMyproducts() {
            this.router.navigate(['/myproducts']);
          }
        }]);

        return MyproductEditPage;
      }();

      _MyproductEditPage.ctorParameters = function () {
        return [{
          type: _shared_http_service__WEBPACK_IMPORTED_MODULE_2__.HttpService
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__.Router
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__.ActivatedRoute
        }];
      };

      _MyproductEditPage = (0, tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-myproduct-edit',
        template: _raw_loader_myproduct_edit_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_myproduct_edit_page_scss__WEBPACK_IMPORTED_MODULE_1__["default"]]
      })], _MyproductEditPage);
      /***/
    },

    /***/
    28191:
    /*!****************************************!*\
      !*** ./src/app/shared/http.service.ts ***!
      \****************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "HttpService": function HttpService() {
          return (
            /* binding */
            _HttpService
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! tslib */
      64762);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common/http */
      91841);
      /* harmony import */


      var _environments_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ../../environments/environment */
      92340);

      var _HttpService = /*#__PURE__*/function () {
        function HttpService(http) {
          _classCallCheck(this, HttpService);

          this.http = http;
        }

        _createClass(HttpService, [{
          key: "get",
          value: function get(serviceName) {
            var userdetails = JSON.parse(atob(localStorage.getItem("24hrs-user-data")));
            var url = _environments_environment__WEBPACK_IMPORTED_MODULE_0__.environment.baseUrl + serviceName;
            var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__.HttpHeaders() // .set("Access-Control-Allow-Origin", "*")
            // .set("Access-Control-Allow-Methods", "GET,POST")
            // .set('Accept','application/json')
            // .set('Content-Type','application/json')
            // .set("Access-Control-Allow-Headers", "Token, Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With")
            .set("Token", userdetails.token); //   let httpOptions = {
            //     headers: new HttpHeaders({
            //       "Access-Control-Allow-Origin": "*",
            //       "Access-Control-Allow-Methods": "*",
            //       "Access-Control-Allow-Headers":"Token, Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With",
            //       'Authorization':userdetails.token,
            //       "Content-Type": "application/json"
            //     })
            // };

            var options = {
              headers: header,
              withCredentials: true
            };
            return this.http.get(url, {
              headers: header
            });
          }
        }, {
          key: "post",
          value: function post(serviceName, data) {
            var token = localStorage.getItem("token");
            var url = _environments_environment__WEBPACK_IMPORTED_MODULE_0__.environment.baseUrl + serviceName;

            if (serviceName == '/user_login' || serviceName == '/user_register') {
              var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__.HttpHeaders();
              var options = {
                headers: headers,
                withCredentials: false
              };
              return this.http.post(url, JSON.stringify(data), {
                headers: headers
              });
            } else {
              var _headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__.HttpHeaders().set("Token", token);

              var _options = {
                headers: _headers,
                withCredentials: true
              };
              return this.http.post(url, JSON.stringify(data), {
                headers: _headers
              });
            }
          }
        }]);

        return HttpService;
      }();

      _HttpService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__.HttpClient
        }];
      };

      _HttpService = (0, tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_3__.Injectable)({
        providedIn: 'root'
      })], _HttpService);
      /***/
    },

    /***/
    52677:
    /*!*********************************************************!*\
      !*** ./src/app/myproduct-edit/myproduct-edit.page.scss ***!
      \*********************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "input[type=text] {\n  font-size: 15px;\n  margin: 0 5px;\n  width: 90%;\n  box-sizing: border-box;\n  border-radius: 5px;\n  background-color: #fff;\n  border: 2px solid #ccc;\n}\n\ninput:focus {\n  outline: none !important;\n  border: 1px solid #FF6000 !important;\n  box-shadow: 0 0 2px #8b8d91;\n}\n\n.card {\n  background-color: #fff;\n  border: 1px solid #fff;\n  border-radius: 8px;\n  width: 96% !important;\n  margin: 5px;\n  box-shadow: 2px 2px 2px 2px #ccc !important;\n  border: 1px solid #fff;\n}\n\n.delete-btn {\n  background-color: #f7391b !important;\n  color: #fff;\n  text-align: center;\n}\n\n.update-btn {\n  background-color: #29C17E;\n  color: #fff;\n  text-align: center;\n}\n\n.btn {\n  margin: 10px 0 !important;\n}\n\nion-content {\n  background-color: #F4F7FA !important;\n  --offset-bottom: auto!important;\n  --overflow: hidden;\n  overflow: auto;\n}\n\nion-content::-webkit-scrollbar {\n  display: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm15cHJvZHVjdC1lZGl0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGVBQUE7RUFDQSxhQUFBO0VBQ0EsVUFBQTtFQUNBLHNCQUFBO0VBQ0Esa0JBQUE7RUFDQSxzQkFBQTtFQUNBLHNCQUFBO0FBQ0o7O0FBQ0U7RUFDRSx3QkFBQTtFQUNBLG9DQUFBO0VBQ0EsMkJBQUE7QUFFSjs7QUFBQTtFQUNJLHNCQUFBO0VBQ0Esc0JBQUE7RUFDQSxrQkFBQTtFQUNBLHFCQUFBO0VBQ0EsV0FBQTtFQUdBLDJDQUFBO0VBQ0Esc0JBQUE7QUFHSjs7QUFEQTtFQUNFLG9DQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0FBSUY7O0FBRkE7RUFDSSx5QkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtBQUtKOztBQUhBO0VBQ0UseUJBQUE7QUFNRjs7QUFKQTtFQUNFLG9DQUFBO0VBS0EsK0JBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7QUFHRjs7QUFGRTtFQUNJLGFBQUE7QUFJTiIsImZpbGUiOiJteXByb2R1Y3QtZWRpdC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpbnB1dFt0eXBlPXRleHRdIHtcbiAgICBmb250LXNpemU6IDE1cHg7XG4gICAgbWFyZ2luOiAwIDVweDtcbiAgICB3aWR0aDogOTAlO1xuICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XG4gICAgYm9yZGVyOiAycHggc29saWQgI2NjYztcbiAgfVxuICBpbnB1dDpmb2N1cyB7XG4gICAgb3V0bGluZTogbm9uZSAhaW1wb3J0YW50O1xuICAgIGJvcmRlcjoxcHggc29saWQgI0ZGNjAwMCAhaW1wb3J0YW50O1xuICAgIGJveC1zaGFkb3c6IDAgMCAycHggIzhiOGQ5MTtcbiAgfVxuLmNhcmR7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbiAgICBib3JkZXI6MXB4IHNvbGlkICNmZmY7XG4gICAgYm9yZGVyLXJhZGl1czogOHB4O1xuICAgIHdpZHRoOjk2JSAhaW1wb3J0YW50O1xuICAgIG1hcmdpbjo1cHg7XG4gICAgLW1vei1ib3gtc2hhZG93OiAwIDAgM3B4ICNjY2M7XG4gICAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDAgM3B4ICNjY2M7XG4gICAgYm94LXNoYWRvdzogMnB4IDJweCAycHggMnB4ICNjY2MgIWltcG9ydGFudDtcbiAgICBib3JkZXI6MXB4IHNvbGlkICNmZmY7XG59XG4uZGVsZXRlLWJ0biB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmNzM5MWIgIWltcG9ydGFudDtcbiAgY29sb3I6I2ZmZjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLnVwZGF0ZS1idG57XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzI5QzE3RTtcbiAgICBjb2xvcjojZmZmO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5idG57XG4gIG1hcmdpbjoxMHB4IDAgIWltcG9ydGFudDtcbn1cbmlvbi1jb250ZW50IHtcbiAgYmFja2dyb3VuZC1jb2xvcjojRjRGN0ZBICAhaW1wb3J0YW50O1xuICAvLyBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQodG8gYm90dG9tLCBkYXJrdHVycXVvaXNlXG4gIC8vIDAlLCAjZDFmMmU3XG4gIC8vIDEwMCUpICFpbXBvcnRhbnQ7XG5cbiAgLS1vZmZzZXQtYm90dG9tOiBhdXRvIWltcG9ydGFudDtcbiAgLS1vdmVyZmxvdzogaGlkZGVuO1xuICBvdmVyZmxvdzogYXV0bztcbiAgJjo6LXdlYmtpdC1zY3JvbGxiYXIge1xuICAgICAgZGlzcGxheTogbm9uZTtcbiAgfVxufSJdfQ== */";
      /***/
    },

    /***/
    47009:
    /*!***********************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/myproduct-edit/myproduct-edit.page.html ***!
      \***********************************************************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header [translucent]=\"true\">\n  <ion-toolbar class=\"new-background-color\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button style=\"margin-top:15px;color:#fff\" autoHide=\"false\"></ion-menu-button>\n    </ion-buttons>\n\n    <div class=\"row\">\n      <div class=\"col-5\">\n        <h5 style=\"color:#fff;margin-top:30px\">24HRS</h5>\n      </div>\n      <div class=\"col-7\" style=\"text-align: right;width: 90%;margin-top: -40px;\">\n        <svg style=\"margin:5px;text-align:right\" width=\"16\" height=\"20\" viewBox=\"0 0 16 20\" fill=\"none\"\n          xmlns=\"http://www.w3.org/2000/svg\">\n          <path fill-rule=\"evenodd\" clip-rule=\"evenodd\"\n            d=\"M8.0002 0C7.11654 0 6.4002 0.716344 6.4002 1.6V1.80156C3.63963 2.51189 1.5998 5.01775 1.5998 8.00003V13.6H2C0.895431 13.6 0 14.4955 0 15.6V15.8C0 16.3523 0.447715 16.8 1 16.8H15C15.5523 16.8 16 16.3523 16 15.8V15.6C16 14.4955 15.1046 13.6 14 13.6H14.3998V8.00003C14.3998 5.01803 12.3604 2.51237 9.60019 1.80176V1.6C9.60019 0.716344 8.88385 0 8.0002 0ZM5.5998 17.6C5.5998 18.9255 6.67432 20 7.9998 20C9.32529 20 10.3998 18.9255 10.3998 17.6H5.5998Z\"\n            fill=\"white\" />\n        </svg>\n      </div>\n    </div>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"bg-class\" [fullscreen]=\"true\">\n  <div class=\"container\">\n    <div class=\"signin-div mt-2\">\n      <div class=\"row mt-2\" style=\"padding: 10px;\">\n        <h5 class=\"name  ion-text-left\" style=\"color:#676767;\"><b>\n            <svg (click)=\"backToMyproducts()\" style=\"margin-top:-8px;cursor:pointer;margin:5px;\"\n              xmlns=\"http://www.w3.org/2000/svg\" width=\"22\" height=\"20\" fill=\"#FF6000\" class=\"bi bi-arrow-left\"\n              viewBox=\"0 0 16 16\">\n              <path fill-rule=\"evenodd\"\n                d=\"M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z\" />\n            </svg>\n            Edit Your Product</b></h5>\n        <div class=\"col-6  ion-text-center\">\n          <p>Product Image:</p>\n          <img height=\"95px\" width=\"95px\" src=\"assets/onion.PNG\" alt=\"\">\n        </div>\n        <div class=\"col-6 ion-text-center\">\n          <p>Change Image:</p>\n          <lottie-player (click)=\"getPicture()\" src=\"https://assets4.lottiefiles.com/private_files/lf30_nzsbtndw.json\"\n            background=\"transparent\" speed=\"1.5\" style=\"width:80%;height:70%\" autoplay></lottie-player>\n        </div>\n        <div class=\"col-12\">\n          <p class=\"name mt-2 ion-text-left\" style=\"color:hsl(0, 0%, 25%);\">Category :</p>\n        </div>\n        <div class=\"col-12 \">\n          <div class=\"select\">\n            <select (change)=\"popupModelOpen()\" [(ngModel)]=\"Category\" placeholder=\"category\">\n              <option value=\"\" disabled selected>Select Your Category</option>\n              <option *ngFor=\"let cat of categoryList\" value=\"{{cat.category}}\">{{cat.category}}</option>\n            </select>\n          </div>\n        </div>\n        <div class=\"col-12 mt-2\">\n          <div class=\"select\">\n            <select (change)=\"subcategoryPopupModelOpen()\" [(ngModel)]=\"subcategory\">\n              <option value=\"\" disabled selected>Select Your Subcategory</option>\n              <option *ngFor=\"let subcat of subcategoryList\" value=\"{{subcat.subsubcategory}}\">{{subcat.subsubcategory}}\n              </option>\n            </select>\n          </div>\n        </div>\n        <div class=\"col-6 mt-3\">\n          <p class=\"name\" style=\"color:#404040;\">Product Name :</p>\n        </div>\n        <div class=\"col-6 mt-2\">\n          <input [(ngModel)]=\"productname\" type=\"text\" placeholder=\"Onion\" id=\"offer\">\n        </div>\n\n        <div class=\"col-6\">\n          <p class=\"name\" style=\"color:#404040;\">Cost:</p>\n          <input [(ngModel)]=\"cost\" type=\"text\" placeholder=\"100\" id=\"offer\">\n        </div>\n        <div class=\"col-6\">\n          <div class=\"select mt-4\" id=\"kg-dropdwon\">\n            <select>\n              <option value=\"\" disabled selected> Kg/g</option>\n              <option class=\"option\" value=\"1\">Kg</option>\n              <option value=\"2\">G</option>\n              <option value=\"3\">L</option>\n              <option value=\"3\">ml</option>\n            </select>\n          </div>\n        </div>\n      </div>\n      <div class=\"form-group \">\n        <label style=\"color:#404040;\" for=\"exampleFormControlTextarea1\">Product Description :</label>\n        <textarea [(ngModel)]=\"description\" style=\"background-color: #fff;\" class=\"form-control\"\n          id=\"exampleFormControlTextarea1\" rows=\"3\"></textarea>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-6 ion-text-center\">\n          <button (click)=\"addToProduct()\" class=\"btn btn-sm delete-btn mt-2\">Delete</button>\n        </div>\n        <div class=\"col-6 ion-text-center\">\n          <button class=\"btn btn-sm update-btn\">Update</button>\n        </div>\n      </div>\n    </div>\n  </div>\n</ion-content>";
      /***/
    }
  }]);
})();
//# sourceMappingURL=src_app_myproduct-edit_myproduct-edit_module_ts-es5.js.map